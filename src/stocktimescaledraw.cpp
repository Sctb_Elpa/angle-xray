#include <QTime>

#include "stocktimescaledraw.h"

StockTimeScaleDraw::StockTimeScaleDraw(const QString& fmt) :
    format(fmt)
{
}

QwtText StockTimeScaleDraw::label(double v) const
{
    QTime t = QTime().addSecs((int)v);
    return t.toString(format);
}


