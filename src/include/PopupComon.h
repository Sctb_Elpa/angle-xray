#ifndef GOPOPUP_H
#define GOPOPUP_H

#include <QWidget>

class AngleEditor;
class QPushButton;
class QLabel;
class AngleConverter;

class PopupComon : public QWidget
{
    Q_OBJECT
public:
    explicit PopupComon(QWidget *parent = 0);
    
    QLabel* lbl;
    AngleEditor *editor;
    QPushButton *btn;

signals:
    void confirmed(const AngleConverter& val);

private slots:
    void bthPressed();
};

#endif // GOPOPUP_H
