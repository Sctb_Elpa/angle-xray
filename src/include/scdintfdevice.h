#ifndef SCDINTFDEVICE_H
#define SCDINTFDEVICE_H

#include <QObject>
#include <QMap>

#include "ScdIntf.h"

class ScdIntfdevice : public QObject
{
    Q_OBJECT
public:

    struct Measure
    {
        double time;
        int pulseNo;
    };

    explicit ScdIntfdevice(int portNum, QObject *parent = 0);
    ~ScdIntfdevice();

    enum Mode getMode();
    double setExposureTime();
    int getNumberOfPulses();
    double getTimeConst();
    int getVoltage();
    int setLowerThreshold();
    int setUpperThreshold();
    bool isIntegralMode();
    bool isSoundEnabled();
    bool isCorrectionEnabled();

    bool isDataReady();
    Measure lastMeasureData();

    QString err2sttr(errCode code) const;
    bool isOk;

public slots:
    void setTimeConst(double timeconst);
    void setVoltage(int voltage);
    void setLowerThreshold(int value);
    void setUpperThreshold(int value);
    void setIntegralMode(bool enable);
    void setSoundMode(bool enableSound);

    void reset();
    void Setup();

    void Start();
    void Stop();

signals:
    void error(errCode code);
    void error_str(const QString& errStr);

    void data(const ScdIntfdevice::Measure& measure);
    void started();
    void stopped();

private:
    struct Scd *thisScd;
    static QMap<struct Scd*, ScdIntfdevice*> mapScd2class;

    static void MeasureStartCallback(struct Scd* ScdHandle) __attribute__((__stdcall__));
    static void MeasureStopCallback(struct Scd* ScdHandle) __attribute__((__stdcall__));
    static void DataProcCallback(struct Scd* ScdHandle, double time, int PulseNo) __attribute__((__stdcall__));
    void reloadSettings();
    void readCfg();
};

#endif // SCDINTFDEVICE_H
