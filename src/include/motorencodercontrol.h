#ifndef MOTORENCODERCONTROL_H
#define MOTORENCODERCONTROL_H

#include <QThread>
#include <QMutex>
#include <QQueue>

#if USE_FTDI_DRIVER == 1
    #include <windows.h>
    #include "ftd2xx.h"
#else
    #include <modbus/modbus.h>
#endif

#define DEFAULT_WORM_GEAR_RATIO         (1) // оборотов / градус
#define DEFAULT_BELT_DRIVE_RATIO        (2) // d[шкива на валу] / d[шкива мотора]

#define ES_D508_MODBUS_ADRESS           (1)

#define ES_D508_RESOLUTION_ADDR         (0x000e)
#define ES_D508_SENSOR_RESOLUTION_ADDR  (0x000f)
#define ES_D508_RESOLUTION_MIN          (200)//realy = (200)
#define ES_D508_RESOLUTION_MAX          (51200)

//#define RESOLUTION_MIN                  (ES_D508_RESOLUTION_MIN * BeltDriveRatio)
#define RESOLUTION_MIN                  (MotorEncoderControl::getFreeMoveRes() * BeltDriveRatio)
#define RESOLUTION_MAX                  (ES_D508_RESOLUTION_MAX * BeltDriveRatio)

class ModbusEngine;
class AngleConverter;

class MotorEncoderControl : public QThread
{
    Q_OBJECT
public:
    explicit MotorEncoderControl(const QString port, QObject *parent = 0);
    ~MotorEncoderControl();

    bool test();
    double curentPos() const; // текущая позиция
    static int getFreeMoveRes();
    
signals:
    void AngleChanged(double newAngle); // новый угол
    void MoveFinished();

public slots:
    void doAngle(const double dAngle, double minRes = -1.0, bool forceSlow = false); // Сдвинуться на угол
    void doAngle(const AngleConverter &dAngle, double minRes = -1.0, bool forceSlow = false); // Сдвинуться на угол

    double setResolution(double ticPreDegree); // Установить количество тиков на реальный градус [меньше - быстрее, но точность падает]
    double resolution() const; // тиков на градус (С учетом трансмиссии)
    void SetCurentPos(double Angle); // установить текущую позицию в градусах
    void SetCurentPos(const AngleConverter &Angle);

    void setWormGearRatio(double AnglePreTurn); // червяк оборотов/градус
    void setBeltDriveRatio(double ratio); // коэфициент ременной пердачи (мал/больш)
    void stop(); // останов

private:
    struct moveCommand
    {
        double minResolution;
        double destination;
        bool forceSlow;
    };

    double curentPosition; // РЕАЛЬНАЯ ПОЗИЦИЯ
    bool fStop;
    double WormGearRatio, BeltDriveRatio;
    unsigned int MotorResolution;

    double trnasmissionRatio() const; // результирующее предаточное отношение мотор - выход (оборотов мотора на градус хода)
    double motorHysteresis() const;

    ModbusEngine *modbus;

#if USE_FTDI_DRIVER == 1
    FT_HANDLE ftHandle;
#else
    modbus_t* ctx;
#endif

    mutable QMutex mutex;
    QQueue<moveCommand> queue;

    bool lastSide;

    void controlPrepare();
    void controlFinish();
    void setStep(bool val);
    void setDir(bool val);

protected:
    void run();
};

#endif // MOTORENCODERCONTROL_H
