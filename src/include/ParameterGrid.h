/*
 * ParameterGrig.h
 *
 *  Created on: 30.01.2012
 *      Author: shiloxyz
 */

#ifndef PARAMETERGRIG_H_
#define PARAMETERGRIG_H_

#include <QWidget>

class QLabel;
class QSlider;
class QSpinBox;

class ParameterGrid: public QWidget
    {
Q_OBJECT

public:
    ParameterGrid(QString& labelText, int DefaultValue = 0);
    ParameterGrid(QString labelText, int DefaultValue = 0);
    virtual ~ParameterGrid();

    bool SetLimits(int Min, int Max);

    QSlider* slider;
    QLabel* label;
    QSpinBox* spinbox;

    int value(void);

public slots:
    void SetValue(int Val);
    void setEnabled(bool fEnabled);
    void setEnabled(int iEnabled);
    void setSingleStep(int step);

signals:
    void valueChanged(int);

private:
    void comonconstructor(int DefaultValue);
    };

#endif /* PARAMETERGRIG_H_ */
