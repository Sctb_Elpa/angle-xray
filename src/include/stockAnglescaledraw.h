#ifndef STOCKANGLESCALEDRAW_H
#define STOCKANGLESCALEDRAW_H

#include <qwt/qwt_scale_draw.h>

class StockAngleScaleDraw : public QwtScaleDraw
{
public:
    StockAngleScaleDraw();

    virtual QwtText label(double v) const;
};

#endif // STOCKANGLESCALEDRAW_H
