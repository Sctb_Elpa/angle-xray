#ifndef ANGLEEDITOR_H
#define ANGLEEDITOR_H

#include <QLineEdit>

class QRegExpValidator;
class AngleConverter;

class AngleEditor : public QLineEdit
{
    Q_OBJECT
public:
    explicit AngleEditor(QWidget *parent = 0);
    AngleConverter value() const;

signals:
    void enterPressed();

public slots:
    void setValue(const AngleConverter& val);

protected:
    void keyPressEvent(QKeyEvent *e);

private:
    QRegExpValidator* validator;

    void modifyGroup(int group, int value);

private slots:
    void onTextChanged(QString txt);
};

#endif // ANGLEEDITOR_H
