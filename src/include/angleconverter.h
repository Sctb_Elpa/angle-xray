#ifndef ANGLECONVERTER_H
#define ANGLECONVERTER_H

#include <stdint.h>

class QString;

class AngleConverter
{
public:
    AngleConverter();
    AngleConverter(double angle);
    AngleConverter(int deg, uint8_t min = 0, double sec = 0, bool forseMinus = false);

    static AngleConverter parce(const QString& str);

    double getAngle() const;
    int getDegs() const;
    uint8_t getMin() const;
    double getSec() const;

    void addDegs(double degs);
    void addMins(double mins);
    void addSecs(double secs);

    QString toString() const;

private:
    double angle;

    void calc(int *deg, int *min, double *sec, bool* minus) const;
};

#endif // ANGLECONVERTER_H
