#ifndef IR2SETUPWINDOW_H
#define IR2SETUPWINDOW_H

#include <QDialog>

class ParameterGrid;
class QLabel;
class QCheckBox;
class QDoubleSpinBox;
class QSlider;

class IR2SetupWindow : public QDialog
{
    Q_OBJECT
public:
    explicit IR2SetupWindow(QWidget *parent = 0);
    double getTimeConst() const;
    unsigned int getWindowLowThreshold() const;
    unsigned int getWindowHighThreshold() const;
    bool isIntegralMode() const;
    unsigned int getVoltage() const;

public slots:
    bool setTimeConst(double timeconst);
    bool setWindowLowThreshold(unsigned int value);
    bool setWindowHighThreshold(unsigned int value);
    bool setIntegralMode(bool enable);
    bool setVoltage(unsigned int voltage);

private:
    QLabel
    *IntegralModeDesc,
    *TimeConstantDesc;

    QCheckBox
    *IntegralMode;

    QDoubleSpinBox
    *TimeConstantSB;

    QSlider
    *TimeConstantSL;

    ParameterGrid
    *Voltage,
    *WindowLowThreshold,
    *WindowHighThreshold;

private slots:
    void IntegralModeSelected(bool selected);
    void LowThresholdChanged(int value);
    void HighThresholdChanged(int value);
    void TimeConstantSBvalChanged(double value);
    void TimeConstantSLvalChanged(int value);
};

#endif // IR2SETUPWINDOW_H
