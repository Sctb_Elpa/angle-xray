#ifndef SOLVER_H
#define SOLVER_H

#include <QMap>

class solver
{
public:
    virtual double solve(double input);
    virtual void reset();
    virtual QMap<QString, double> getCoeffs() const;
    virtual bool setCoeffs(const QMap<QString, double> &coeffs);
};

#endif // SOLVER_H
