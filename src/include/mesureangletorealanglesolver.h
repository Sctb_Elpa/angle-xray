#ifndef MESUREANGLETOREALANGLESOLVER_H
#define MESUREANGLETOREALANGLESOLVER_H

#include "solver.h"

#include <QMap>
#include <QString>

class MesureAngleToRealAngleSolver : public solver
{
public:
    MesureAngleToRealAngleSolver();
    double solve(double input);
    bool setCoeffs(const QMap<QString, double> &coeffs);
    void reset();

private:
    QMap<QString, double> coeffs;
};

#endif // MESUREANGLETOREALANGLESOLVER_H
