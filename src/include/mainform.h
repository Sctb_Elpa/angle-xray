#ifndef MAINFORM_H
#define MAINFORM_H

#include <QMainWindow>
#include <scdintfdevice.h>
#include <QList>

class QPushButton;
class MotorEncoderControl;
class ScdIntfdevice;
class QAction;
class Plot;
class ManualMoveWindow;
class AngleController;
class QLabel;
class QTimer;
class QPoint;

class Mainform : public QMainWindow
{
    Q_OBJECT
public:
    explicit Mainform(MotorEncoderControl* mec, ScdIntfdevice* scd, QWidget *parent = 0);
    
    bool eventFilter(QObject *obj, QEvent *e);
    
public slots:
    void StartStop();
    void StartStop(bool start);

private:
    MotorEncoderControl *MEC;
    ScdIntfdevice *IR2;

    QAction *IR2Setup, *MECSetup, *manualWork, *clearPlot;
    QList<QAction*> WorkVariants;
    QAction *StopAction, *ResetAction;
    QList<QAction*> stages;

    QPushButton* startStopBtn;
    AngleController* angleController;

    void createMenus();
    void createToolbars();
    void connectSignalsAndSlots();

    Plot* plot;

    enum stage
    {
        IDLE = 0,
        PRESTART = 1,
        FORVARD_QUICK = 2,
        BACK1 = 3,
        FORVARD_NORMAL = 4,
        BACK2 = 5,
        FORVARD_SLOW = 6,
        MANUAL = 0xff
    };

    stage currentStage;

    ManualMoveWindow* mmWin;

    QLabel* CurrentPosToolbarLabel, *ResultLabel;
    QTimer* updatetimer;

    void SetButton(bool startStop);
    double mesureStepAcurasy() const;

private slots:
    void measureResived(ScdIntfdevice::Measure m);
    void disableManualControl();
    void MoveFinished();
    void WorkTypeChanged();
    void UpdatePosOnToolbar();
    void GotoZero();
    void ResetCurrentPos();
    void EditCurentPos();
    void StagesChanged();
};

#endif // MAINFORM_H
