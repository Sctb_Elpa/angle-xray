#ifndef ANGLECONTROLLER_H
#define ANGLECONTROLLER_H

#include <QStringList>

class solver;

class AngleController
{
public:
    enum enCutType
    {
        UST = 0,
        X_18 = 1,
        Y_7,
        Y_10,
        Z_22,
        Z_25,
        AT_13,
        AT_10,

        VARIANTS_COUNT = AT_10
    };

    AngleController(enCutType cutType);
    virtual ~AngleController();

    double AproxAngle() const;
    double StartAngle() const;
    double EndAngle() const;
    double SearchArc() const;
    double getPassError() const;

    double convertToCutAngle(double mesure);
    QString convertToCutRes(double mesure);

private:
    enCutType cutType;

    QStringList settingsNames;

    solver* convertor;
    double passError;
};

#endif // ANGLECONTROLLER_H
