#ifndef MANUALMOVEWINDOW_H
#define MANUALMOVEWINDOW_H

#include <QDialog>


class AngleEditor;
class QPushButton;
class MotorEncoderControl;

class ManualMoveWindow : public QDialog
{
    Q_OBJECT
public:
    explicit ManualMoveWindow(MotorEncoderControl* mec, QWidget *parent = 0);
    
private:
    AngleEditor * currentposEdit;
    AngleEditor * destinationEdit;
    QPushButton *GoButton, *stopButton, *ResetButton;
    MotorEncoderControl *mec;

private slots:
    void currentPosChanged(double pos);
    void GoSlot();
    void resetSlot();
    void currentPosEdited();
};

#endif // MANUALMOVEWINDOW_H
