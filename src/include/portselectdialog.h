#ifndef PORTSELECTDIALOG_H
#define PORTSELECTDIALOG_H

#include <QDialog>

class QComboBox;

class PortSelectDialog : public QDialog
{
    Q_OBJECT
public:
    explicit PortSelectDialog(QWidget *parent = 0);
    void addItem(const QString &text);

    void sellectItem(const QString& text);
    QString selectedItem() const;

private:
    QComboBox* combobox;
};

#endif // PORTSELECTDIALOG_H
