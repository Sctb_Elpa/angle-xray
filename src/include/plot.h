#ifndef PLOT_H
#define PLOT_H

#include <qwt/qwt_plot.h>
#include <QList>
#include <QColor>
#include <QVector>
#include <qwt/qwt_symbol.h>

class QwtPlotCurve;
class QwtSymbol;

class Plot : public QwtPlot
{
    Q_OBJECT
public:
    explicit Plot(QWidget *parent = 0);

    static Plot* fromData(const QByteArray &d);
    static QVector<QPointF> buildSmoothData(const QVector<double>& srcX, const QVector<double>& srcY, QPointF *max = NULL);
    double BuildSmoothCurve(QwtPlotCurve *curve = NULL);
    QwtPlotCurve* getCurrentcurve() const;
    double lastMaximum() const;
    
public slots:
    void startNewCurve(const QColor& color = QColor(Qt::black));
    void addPoint(double X, int Y);
    void SetData(const QVector<double>& X, const QVector<double>& Y);
    void setTimeMode(bool TimeMode);
    void clear();
    void SaveData();

private:
    QVector<double> dataX, dataY;
    QwtPlotCurve* currentcurve;
    QList<QwtPlotCurve*> raw;
    QList<QwtPlotItem*> plotItems;

    QwtPlotCurve* builCurve(const QColor &color, const QwtSymbol::Style symbolStyle, int penWidth);
    QwtPlotCurve* buildVertLine(const QColor &color, const QPointF& pos);

    double LastMaximum;
};

#endif // PLOT_H
