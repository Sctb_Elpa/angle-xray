#ifndef STOCKTIMESCALEDRAW_H
#define STOCKTIMESCALEDRAW_H

#include <qwt/qwt_scale_draw.h>

class StockTimeScaleDraw : public QwtScaleDraw
{
public:
    StockTimeScaleDraw(const QString &fmt);

    virtual QwtText label(double v) const;

private:
    const QString format;
};

#endif // STOCKTIMESCALEDRAW_H
