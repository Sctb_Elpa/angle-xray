#ifndef ATRESULTCONVERTER_H
#define ATRESULTCONVERTER_H

#include <QMap>
#include <QString>

#include "solver.h"

class AtResultConverter : public solver
{
public:
    AtResultConverter();

    double solve(double input);
    void reset();
    QMap<QString, double> getCoeffs() const;
    bool setCoeffs(const QMap<QString, double> &coeffs);

private:
    QMap<QString, double> coeffs;
};

#endif // ATRESULTCONVERTER_H
