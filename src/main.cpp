#include <QApplication>
#include <QAction>

#include <scdintfdevice.h>
#include <mylog/mylog.h>
#include <Settings.h>
#include <QFile>
#include <QTimer>
#include <QDateTime>

#include <SerialDeviceEnumerator.h>

#include "portselectdialog.h"
#include "motorencodercontrol.h"
#include "mainform.h"
#include "scdintfdevice.h"
#include "plot.h"

#define _FT_VID     "0403"
#define _FT_PID     "6001"

#define _FT_ZVERCD_VID     "FTDIBUS\\COMPORT&VID_0403&PID_6001"
#define _FT_ZVERCD_PID     "FTDIBUS\\COMPORT&VID_0403&PID_6001"

int main(int argc, char *argv[])
{
    int res = 0;
    ScdIntfdevice* scd = NULL;
    Mainform *m = NULL;

    QApplication app(argc, argv);
    QIcon appIcon(":/res/X-ray.png");
    app.setApplicationName("Angle-Xray");

    Settings _settings(argc, argv);

    MotorEncoderControl* mec = NULL;

    MyLog::myLog log;
#ifndef NODEBUG
    log.setLogLevel(MyLog::LOG_DEBUG);
    log.setLogFileName(QObject::trUtf8("res/log/%1.log"));
    //LOG_DEBUG(QObject::trUtf8("res/log/%0.log").arg(
    //              QDateTime::currentDateTime().toMSecsSinceEpoch()));
#endif

    if (_settings.value("testfile").toString().isEmpty())
    {
        // Список доступных портов
        SerialDeviceEnumerator* enumerator = SerialDeviceEnumerator::instance();
        QStringList avalable = enumerator->devicesAvailable();
        QStringList ftAvalale = avalable;
        ftAvalale.detach();

        foreach(QString portname, ftAvalale)
        {
            enumerator->setDeviceName(portname);
            LOG_DEBUG(QObject::trUtf8("Checking %1: VID: %2\tPID:%3").arg(portname).arg(enumerator->vendorID()).arg(enumerator->productID()));
            if (!((enumerator->vendorID() == _FT_VID) || (enumerator->vendorID() == _FT_ZVERCD_VID)) ||
                    !((enumerator->productID() == _FT_PID) || (enumerator->productID() == _FT_ZVERCD_PID)) ||
                    (enumerator->isBusy() == true))
                ftAvalale.removeOne(portname);
            else
                if (portname.at(0) != '/')
                {
                    QString number = portname;
                    number.remove(0, 3);
                    if (number.toInt() > 9)
                    {
                        ftAvalale.removeOne(portname);
                        ftAvalale.append("\\\\.\\" + portname);
                    }
                }
        }
        enumerator->deleteLater();

        if (avalable.isEmpty() || ftAvalale.isEmpty())
        {
            LOG_ERROR("No serial ports avalable, exiting");
            return 1;
        }

        LOG_DEBUG(QString("Avalable ports: %1").arg(avalable.join(", ")));
        LOG_DEBUG(QString("FTDI ports: %1").arg(ftAvalale.join(", ")));

        LOG_INFO("Trying to detect  Motor-encoder device..");
        foreach(QString port, ftAvalale)
        {
            mec = new MotorEncoderControl(port);
            if (mec->test())
            {
                LOG_INFO(QObject::trUtf8("Motor-encoder found on port %1").arg(port));
                break;
            }
            else
            {
                LOG_WARNING(QObject::trUtf8("Motor-encoder not found on port %1").arg(port));
                delete mec;
                mec = NULL;
            }
        }
        // нашли мотор?
        if (!mec)
        {
            LOG_ERROR("No Motor-encoder devices detected, exiting");
            return 1;
        }

        //mec->doAngle(-1);
        LOG_DEBUG(QObject::trUtf8("Curent resolution: %1").arg(mec->resolution()));
        mec->setResolution(1250);
        //mec->doAngle(-1.53);

        PortSelectDialog portselectdialog;
        portselectdialog.setWindowIcon(appIcon);
        foreach(QString port, avalable)
        {
            if (ftAvalale.contains(port))
                continue;
            else
                portselectdialog.addItem(port);
        }
        portselectdialog.sellectItem(SettingsCmdLine::settings->value("IR-2/Portname").toString());
        portselectdialog.exec();

        QString IR_2port = portselectdialog.selectedItem();
        LOG_INFO(QObject::trUtf8("%1 selected as port for IR-2").arg(IR_2port));
        (*SettingsCmdLine::settings)["IR-2/Portname"] = IR_2port;

        scd = new ScdIntfdevice(IR_2port.split("M").at(1).toInt()); // только цыфра от COMx
        if (!scd->isOk)
        {
            LOG_ERROR("Failed to connect to open connection to IR-2, exiting");
            return 1;
        }
    }
    else
    {
        QFile data(_settings.value("testfile").toString());
        if (data.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            Plot* plot = Plot::fromData(data.readAll());
            plot->resize(800, 600);
            plot->show();
            //QTimer::singleShot(10, plot, SLOT(replot()));
            res = app.exec();
            delete plot;
            return 0;
        }
        return -1;
    }

    m = new Mainform(mec, scd);
    m->setWindowIcon(appIcon);
    m->show();

    res = app.exec();

    delete m;
    delete mec;

    return res;
}
