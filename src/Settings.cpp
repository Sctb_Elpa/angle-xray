#include <cstdio>
#include <iostream>

#include <QIODevice>

#include <mylog/mylog.h>

#include "motorencodercontrol.h"

#include "Settings.h"


/// Типы ключей
enum enArgs
{
    OPT_HELP,
    OPT_TEST
};

/// Таблица ключей программы
static CSimpleOpt::SOption g_rgOptions[] =
{
    {
        OPT_HELP, _T("-h"), SO_NONE
    }, /// "-h"
    {
        OPT_HELP, _T("--help"), SO_NONE
    }, /// "--help"
    {
        OPT_TEST, _T("-t"), SO_REQ_SEP
    }, /// "-t"
    SO_END_OF_OPTIONS
    // END
};

/// Таблица указателей
SettingsCmdLine::pfTable Settings::table =
{
    g_rgOptions,
    Settings::SetDefaultValues,
    Settings::console_ShowUsage,
    Settings::parse_Arg
};

void Settings::console_ShowUsage(cSettingsCmdLine* _this)
{
    std::cout
            << trUtf8(
                   "Usage: LaserControl-ng [-h | --help]\n\
                   \n").toLatin1().data();
}

struct SettingsCmdLine::key_val_res Settings::parse_Arg(int optCode,
          const char* optText, char *ArgVal, cSettingsCmdLine* origins)
{
    SettingsCmdLine::key_val_res result =
    {
            QString(), QVariant(), true
    };
    switch (optCode)
    {
    case OPT_TEST:
        result.key = "testfile";
        result.val = QString(ArgVal);
        break;
    default:
        result.res = false;
        break;
    }
    return result;
}

void Settings::SetDefaultValues(cSettingsCmdLine* _this)
{
    _this->insert("Global/Timeout", 20);
    _this->insert("Global/Baud", 38400);
    _this->insert("UST/Angle", trUtf8("0°"));
    _this->insert("UST/SearchArgSize", 0.5);
    _this->insert("X-18/Angle", trUtf8("18° 18\'"));
    _this->insert("X-18/SearchArgSize", 20 / 60.0); // 20\'
    _this->insert("X-18/WorkPassError", trUtf8("0° 0\' 7\""));
    _this->insert("Z-22/Angle", trUtf8("22° 20\'"));
    _this->insert("Z-22/SearchArgSize", 20 / 60.0); // 20\'
    _this->insert("Z-22/WorkPassError", trUtf8("0° 0\' 10\""));
    _this->insert("Z-25/Angle", trUtf8("25° 20\'"));
    _this->insert("Z-25/SearchArgSize", 20 / 60.0); // 20\'
    _this->insert("Z-25/WorkPassError", trUtf8("0° 0\' 10\""));
    _this->insert("Y-7/Angle", trUtf8("7° 26\'"));
    _this->insert("Y-7/SearchArgSize", 20 / 60.0); // 20\'
    _this->insert("Y-7/WorkPassError", trUtf8("0° 0\' 10\""));
    _this->insert("Y-10/Angle", trUtf8("10° 26\'"));
    _this->insert("Y-10/SearchArgSize", 20 / 60.0); // 20\'
    _this->insert("Y-10/WorkPassError", trUtf8("0° 0\' 10\""));
    _this->insert("AT-13/Angle", trUtf8("13° 19\'"));
    _this->insert("AT-13/SearchArgSize", 10 / 60.0); // 13\'
    _this->insert("AT-13/WorkPassError", trUtf8("0° 0\' 6\""));
    _this->insert("AT-10/Angle", trUtf8("10° 22\'"));
    _this->insert("AT-10/SearchArgSize", 10 / 60.0); // 10\'
    _this->insert("AT-10/WorkPassError", trUtf8("0° 0\' 6\""));
    _this->insert("Mesurment/Acuracy", ES_D508_RESOLUTION_MAX / 10);
    _this->insert("Mesurment/FreeMoveAcuracy", 800);
    _this->insert("Global/MotorHisterezis", trUtf8("0° 0\' 0\""));

}

Settings::Settings(int argc, char *argv[]) :
    QObject(NULL), cSettingsCmdLine(argc, argv, "res/Settings.ini", &table)
  /// параметр 3 устанавливает имя файла настроек программы
{
}

Settings::~Settings()
{
    if (keys().contains("testfile"))
        remove("testfile");
}
