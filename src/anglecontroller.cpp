#include <QObject>

#include <settingscmdline/settingscmdline.h>

#include "angleconverter.h"
#include "mesureangletorealanglesolver.h"
#include "atresultconverter.h"

#include "anglecontroller.h"

AngleController::AngleController(enCutType cutType)
{
    this->cutType = cutType;
    settingsNames << "UST" << "X-18" << "Y-7" << "Y-10" << "Z-22" << "Z-25" << "AT-13" << "AT-10";

    QMap<QString, double> coeffs;
    QString passerror_str = SettingsCmdLine::settings->value(
                settingsNames.at(cutType) + QObject::trUtf8("/WorkPassError"), QObject::trUtf8("0°")
                ).toString();
    passError = AngleConverter::parce(passerror_str).getAngle();

    switch (cutType)
    {
    case UST:
        convertor = new MesureAngleToRealAngleSolver;
        coeffs["k"] = 1;
        coeffs["b"] = 0;
        break;
    case X_18:
        convertor = new MesureAngleToRealAngleSolver;
        coeffs["k"] = -1;
        coeffs["b"] = AngleConverter(18, 18, 0).getAngle();
        break;
    case Y_7:
        convertor = new MesureAngleToRealAngleSolver;
        coeffs["k"] = -1;
        coeffs["b"] = AngleConverter(7, 26, 0).getAngle();
        break;
    case Y_10:
        convertor = new MesureAngleToRealAngleSolver;
        coeffs["k"] = -1;
        coeffs["b"] = AngleConverter(10, 26, 0).getAngle();
        break;
    case Z_25:
        convertor = new MesureAngleToRealAngleSolver;
        coeffs["k"] = -1;
        coeffs["b"] = AngleConverter(25, 20, 10).getAngle();
        break;
    case Z_22:
        convertor = new MesureAngleToRealAngleSolver;
        coeffs["k"] = -1;
        coeffs["b"] = AngleConverter(22, 20, 10).getAngle();
        break;
    case AT_10:
        convertor = new AtResultConverter;
        coeffs["A1"] = AngleConverter(38, 12, 40).getAngle();
        coeffs["A2"] = AngleConverter(13, 19, 10).getAngle();
        break;
    case AT_13:
        convertor = new MesureAngleToRealAngleSolver;
        coeffs["k"] = -1;
        coeffs["b"] = AngleConverter(13, 19, 0).getAngle();
        break;
    }

    convertor->setCoeffs(coeffs);
}

AngleController::~AngleController()
{
    delete convertor;
}

double AngleController::AproxAngle() const
{
    return AngleConverter::parce(
                SettingsCmdLine::settings->value(settingsNames.at(cutType) + "/Angle", 0).toString()
                ).getAngle();
}

double AngleController::StartAngle() const
{
    return AproxAngle() - SearchArc() / 2.0;
}

double AngleController::EndAngle() const
{
    return AproxAngle() + SearchArc() / 2.0;
}

double AngleController::SearchArc() const
{
    return SettingsCmdLine::settings->value(settingsNames.at(cutType) + "/SearchArgSize", 1).toDouble() * 2.0;
}

double AngleController::getPassError() const
{
    return passError;
}

double AngleController::convertToCutAngle(double mesure)
{
    return convertor->solve(mesure);
}

QString AngleController::convertToCutRes(double mesure)
{
    QString res;
    switch (cutType)
    {
    case UST:
        res = QObject::trUtf8("Ошибка установки 0: %1");
        break;
    case AT_10:
        res = QObject::trUtf8("Угол среза: %1");
        break;
    case Z_25:
    case Y_7:
    case Y_10:
    case AT_13:
    case X_18:
    case Z_22:
        res = QObject::trUtf8("Ошибка среза: %1");
        break;
    default:
        break;
    }
    return res.arg(AngleConverter(convertToCutAngle(mesure)).toString());
}
