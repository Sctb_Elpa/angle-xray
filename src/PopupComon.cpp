
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>

#include "angleeditor.h"
#include "angleconverter.h"

#include "PopupComon.h"

PopupComon::PopupComon(QWidget *parent) :
    QWidget(parent)
{
    setWindowFlags(Qt::FramelessWindowHint /*| Qt::Tool*/ | Qt::Popup);
    //setAttribute(Qt::WA_TranslucentBackground);
    setAttribute(Qt::WA_DeleteOnClose);
    //setAutoFillBackground(false);
    //setStyleSheet("background-color: #1b1b1b; border: 1px solid; border-radius: 7px; border-color: #303030;"); // по вкусу
    //setGeometry(сколько надо);

    QVBoxLayout *rootLayout = new QVBoxLayout;
    QHBoxLayout *Layout = new QHBoxLayout;

    lbl = new QLabel("Label");
    editor = new AngleEditor;
    btn = new QPushButton;

    rootLayout->addWidget(lbl);
    Layout->addWidget(editor);
    Layout->addWidget(btn);
    rootLayout->addLayout(Layout);

    setLayout(rootLayout);

    connect(btn, SIGNAL(clicked()), this, SLOT(bthPressed()));
    connect(editor, SIGNAL(enterPressed()), this, SLOT(bthPressed()));

    editor->setFocus();
}


void PopupComon::bthPressed()
{
    emit confirmed(editor->value());
}
