#include <math.h>

#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QLabel>
#include <QSlider>
#include <QDoubleSpinBox>
#include <QIcon>

#include "ParameterGrid.h"
#include "ScdIntf.h"

#include "ir2setupwindow.h"

IR2SetupWindow::IR2SetupWindow(QWidget *parent) :
    QDialog(parent)
{
    setWindowIcon(QIcon(":/res/X-ray.png"));
    QGridLayout* grid = new QGridLayout;
    QVBoxLayout* rootlayout = new QVBoxLayout;
    QHBoxLayout* buttonlauout = new QHBoxLayout;

    rootlayout->addLayout(grid);
    rootlayout->addLayout(buttonlauout);
    setLayout(rootlayout);

    QPushButton* okbtn = new QPushButton(trUtf8("Ok"));
    QPushButton* cancelBtn = new QPushButton(trUtf8("Отмена"));

    buttonlauout->addStretch();
    buttonlauout->addWidget(okbtn);
    buttonlauout->addWidget(cancelBtn);
    buttonlauout->addStretch();

    connect(okbtn, SIGNAL(clicked()), this, SLOT(accept()));
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(reject()));

    IntegralModeDesc = new QLabel(trUtf8("Интегральный режим"));
    TimeConstantDesc = new QLabel(trUtf8("Постоянная времени [c]"));

    IntegralMode = new QCheckBox;

    TimeConstantSB = new QDoubleSpinBox;
    TimeConstantSB->setMinimum(TIME_CONST_MIN);
    TimeConstantSB->setMaximum(TIME_CONST_MAX);

    TimeConstantSL = new QSlider(Qt::Horizontal);
    TimeConstantSL->setMinimum((int)(20 * log(TIME_CONST_MIN)));
    TimeConstantSL->setMaximum((int)(20 * log(TIME_CONST_MAX)));

    Voltage = new ParameterGrid(trUtf8("Напряжение [В]"));
    Voltage->SetLimits(VOLTAGE_MIN, VOLTAGE_MAX);

    WindowLowThreshold = new ParameterGrid(trUtf8("Нижний порог"));
    WindowLowThreshold->SetLimits(THRESHOLD_MIN, THRESHOLD_MAX - 1);

    WindowHighThreshold = new ParameterGrid(trUtf8("Верхний порог"));
    WindowHighThreshold->SetLimits(THRESHOLD_MIN + 1, THRESHOLD_MAX);

    grid->addWidget(TimeConstantDesc, 0, 0);
    grid->addWidget(TimeConstantSB, 0, 1);
    grid->addWidget(TimeConstantSL, 0, 2);

    grid->addWidget(WindowHighThreshold->label, 1, 0);
    grid->addWidget(WindowHighThreshold->spinbox, 1, 1);
    grid->addWidget(WindowHighThreshold->slider, 1, 2);

    grid->addWidget(WindowLowThreshold->label, 2, 0);
    grid->addWidget(WindowLowThreshold->spinbox, 2, 1);
    grid->addWidget(WindowLowThreshold->slider, 2, 2);

    grid->addWidget(IntegralModeDesc, 3, 0);
    grid->addWidget(IntegralMode, 3, 1);

    grid->addWidget(Voltage->label, 4, 0);
    grid->addWidget(Voltage->spinbox, 4, 1);
    grid->addWidget(Voltage->slider, 4, 2);

    connect(IntegralMode, SIGNAL(clicked(bool)), this, SLOT(IntegralModeSelected(bool)));
    connect(WindowLowThreshold, SIGNAL(valueChanged(int)), this, SLOT(LowThresholdChanged(int)));
    connect(WindowHighThreshold, SIGNAL(valueChanged(int)), this, SLOT(HighThresholdChanged(int)));

    connect(TimeConstantSB, SIGNAL(valueChanged(double)), this, SLOT(TimeConstantSBvalChanged(double)));
    connect(TimeConstantSL, SIGNAL(valueChanged(int)), this, SLOT(TimeConstantSLvalChanged(int)));
}

double IR2SetupWindow::getTimeConst() const
{
    return TimeConstantSB->value();
}

unsigned int IR2SetupWindow::getWindowLowThreshold() const
{
    return WindowLowThreshold->value();
}

unsigned int IR2SetupWindow::getWindowHighThreshold() const
{
    return WindowHighThreshold->value();
}

bool IR2SetupWindow::setWindowHighThreshold(unsigned int value)
{
    if ((value > THRESHOLD_MIN) && (value <= THRESHOLD_MAX))
    {
        WindowHighThreshold->SetValue(value);
        return true;
    }
    else
        return false;
}

bool IR2SetupWindow::isIntegralMode() const
{
    return IntegralMode->isChecked();
}

unsigned int IR2SetupWindow::getVoltage() const
{
    return Voltage->value();
}

bool IR2SetupWindow::setTimeConst(double timeconst)
{
    if ((timeconst >= TIME_CONST_MIN) && (timeconst <= TIME_CONST_MAX))
    {
        TimeConstantSB->setValue(timeconst);
        return true;
    }
    else
        return false;
}

bool IR2SetupWindow::setWindowLowThreshold(unsigned int value)
{
    if ((value >= THRESHOLD_MIN) && (value < THRESHOLD_MAX))
    {
        WindowLowThreshold->SetValue(value);
        return true;
    }
    else
        return false;
}

bool IR2SetupWindow::setIntegralMode(bool enable)
{
    IntegralMode->setChecked(enable);
    IntegralModeSelected(enable);
}

bool IR2SetupWindow::setVoltage(unsigned int voltage)
{
    if ((voltage >= VOLTAGE_MIN) && (voltage <= VOLTAGE_MAX))
    {
        Voltage->SetValue(voltage);
        return true;
    }
    else
        return false;
}

void IR2SetupWindow::IntegralModeSelected(bool selected)
{
    WindowHighThreshold->setEnabled(!selected);
}

void IR2SetupWindow::LowThresholdChanged(int value)
{
    if (value >= WindowHighThreshold->value())
    {
        if (WindowHighThreshold->value() == THRESHOLD_MAX)
        {
            WindowLowThreshold->blockSignals(true);
            WindowLowThreshold->SetValue(THRESHOLD_MAX - 1);
            WindowLowThreshold->blockSignals(false);
        }
        else
        {
            WindowHighThreshold->blockSignals(true);
            WindowHighThreshold->SetValue(value + 1);
            WindowHighThreshold->blockSignals(false);
        }
    }
}

void IR2SetupWindow::HighThresholdChanged(int value)
{
    if (value <= WindowLowThreshold->value())
    {
        if (WindowLowThreshold->value() == THRESHOLD_MIN)
        {
            WindowHighThreshold->blockSignals(true);
            WindowHighThreshold->SetValue(THRESHOLD_MIN + 1);
            WindowHighThreshold->blockSignals(false);
        }
        else
        {
            WindowLowThreshold->blockSignals(true);
            WindowLowThreshold->SetValue(value - 1);
            WindowLowThreshold->blockSignals(false);
        }
    }
}

void IR2SetupWindow::TimeConstantSBvalChanged(double value)
{
    TimeConstantSL->blockSignals(true);
    TimeConstantSL->setValue((int)(20 * log(value)));
    TimeConstantSL->blockSignals(false);
}

void IR2SetupWindow::TimeConstantSLvalChanged(int value)
{
    TimeConstantSB->blockSignals(true);
    TimeConstantSB->setValue(exp((double)value / 20));
    TimeConstantSB->blockSignals(false);
}
