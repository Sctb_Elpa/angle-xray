
#include <QRegExp>
#include <QRegExpValidator>
#include <QValidator>
#include <QKeyEvent>
#include <QStringList>
#include <QDebug>

#include "angleconverter.h"

#include "angleeditor.h"

AngleEditor::AngleEditor(QWidget *parent) :
    QLineEdit(parent)
{
    QRegExp condition(trUtf8("^-?\\d*°\\s[0-5]?\\d?\'\\s[0-5]?\\d?\"$"));
    setText(AngleConverter().toString());
    validator = new QRegExpValidator(condition, this);
    connect(this, SIGNAL(textEdited(QString)), this, SLOT(onTextChanged(QString)));
}

void AngleEditor::keyPressEvent(QKeyEvent *e)
{
    int cursorPos = cursorPosition();
    int group;

    if (text().indexOf(trUtf8("°")) >= cursorPos)
        group = 0;
    else
    {
        if (text().indexOf("\'") >= cursorPos)
            group = 1;
        else
            group = 2;
    }

    switch (e->key())
    {
    case Qt::Key_Up:
        modifyGroup(group, +1);
        break;
    case Qt::Key_Down:
        modifyGroup(group, -1);
        break;
    case Qt::Key_Enter:
    case Qt::Key_Return:
        emit enterPressed();
        emit textEdited(text());
        break;
    default:
        QLineEdit::keyPressEvent(e);
        break;
    }
}

AngleConverter AngleEditor::value() const
{
    return AngleConverter::parce(text());
}

void AngleEditor::setValue(const AngleConverter &val)
{
    int savedCursorPos = cursorPosition();
    setText(val.toString());
    setCursorPosition(savedCursorPos);
}

void AngleEditor::modifyGroup(int group, int value)
{
    AngleConverter currentValue = this->value();

    switch (group)
    {
    case 0:
        currentValue.addDegs(value);
        break;
    case 1:
        currentValue.addMins(value);
        break;
    case 2:
        currentValue.addSecs(value);
        break;
    default:
        return;
    }

    setValue(currentValue);
}

void AngleEditor::onTextChanged(QString txt)
{
    int pos = 0;
    if (validator->validate(txt, pos) != QValidator::Acceptable)
        undo();
}
