#include "portselectdialog.h"

#include <QComboBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>

PortSelectDialog::PortSelectDialog(QWidget *parent) :
    QDialog(parent)
{
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(new QLabel(trUtf8("Выберите порт, к которому подключен ИР-2")));
    layout->addWidget(combobox = new QComboBox);
    QPushButton* okbutton = new QPushButton("OK");
    QHBoxLayout* btncenter = new QHBoxLayout;
    btncenter->addStretch();
    btncenter->addWidget(okbutton);
    btncenter->addStretch();
    connect(okbutton, SIGNAL(clicked()), this, SLOT(accept()));
    layout->addLayout(btncenter);
    setLayout(layout);
    combobox->setEditable(false);
    combobox->setDuplicatesEnabled(false);
    setWindowTitle(trUtf8("Настройка соединения"));
}

void PortSelectDialog::addItem(const QString &text)
{
    combobox->addItem(text);
}

void PortSelectDialog::sellectItem(const QString &text)
{
    int index = combobox->findText(text);
    if (index >= 0)
        combobox->setCurrentIndex(index);
}

QString PortSelectDialog::selectedItem() const
{
    return combobox->itemText(combobox->currentIndex());
}
