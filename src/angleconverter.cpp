#include <math.h>
#include <stdio.h>

#include <QObject>

#include "angleconverter.h"

AngleConverter::AngleConverter()
{
    this->angle = 0.0;
}

AngleConverter::AngleConverter(double angle)
{
    this->angle = angle;
}

AngleConverter::AngleConverter(int deg, uint8_t min, double sec, bool forseMinus)
{
    if (min >= 60)
        min = 59;
    if (sec >= 60)
        sec = 59;

    double m;
    if (deg < 0 || forseMinus)
    {
        m = -1.0;
        deg = -deg;
    }
    else
        m = 1.0;
    angle = m * (deg + min / 60.0 + sec / 3600.0);
}

AngleConverter AngleConverter::parce(const QString &str)
{
    int d = 0, m = 0, s = 0;
    if (str.contains('\''))
    {
        if (str.contains('\"'))
            sscanf(str.toAscii().data(), QObject::trUtf8("%i° %i\' %i\"").toAscii().data(), &d, &m, &s);
        else
            sscanf(str.toAscii().data(), QObject::trUtf8("%i° %i\'").toAscii().data(), &d, &m);
    }
    else
        sscanf(str.toAscii().data(), QObject::trUtf8("%i°").toAscii().data(), &d);

    if (str.at(0) == '-')
        return  AngleConverter(d, m, s, true);
    else
        return AngleConverter(d, m, s);
}

double AngleConverter::getAngle() const
{
    return angle;
}

int AngleConverter::getDegs() const
{
    int deg;
    int min;
    double sec;
    bool minus;
    calc(&deg, &min, &sec, &minus);
    return deg;
}

uint8_t AngleConverter::getMin() const
{
    int deg;
    int min;
    double sec;
    bool minus;
    calc(&deg, &min, &sec, &minus);
    return min;
}

double AngleConverter::getSec() const
{
    int deg;
    int min;
    double sec;
    bool minus;
    calc(&deg, &min, &sec, &minus);
    return sec;
}

void AngleConverter::addDegs(double degs)
{
    angle += degs;
}

void AngleConverter::addMins(double mins)
{
    angle += mins / 60.0;
}

void AngleConverter::addSecs(double secs)
{
    angle += secs / 3600.0;
}

QString AngleConverter::toString() const
{
    int deg;
    int min;
    double sec;
    bool minus;
    calc(&deg, &min, &sec, &minus);
    if (fabs(min*10) > 595)
    {
        ++deg;
        min = 0;
    }

    return QObject::trUtf8("%1%2° %3\' %4\"")
            .arg(minus ? "-" : "")
            .arg(deg)
            .arg(min, 2, 10, QChar('0'))
            .arg(sec, 2, 'f', 0, QChar('0'));
}

void AngleConverter::calc(int *deg, int *min, double *sec, bool *minus) const
{
    *minus = angle < 0;
    double a = fabs(angle);
    *deg = floor(a);
    a = (a - *deg) * 60;
    *min = floor(a);
    a = (a - *min) * 60;
    *sec = a;
    if (lround(a) > 59)
    {
        ++(*min);
        *sec -= 60;
        if (*sec < 0.0)
            *sec = 0.0;
    }
}
