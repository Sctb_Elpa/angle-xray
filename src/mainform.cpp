#include <cmath>

#include <QApplication>
#include <QStyle>
#include <QDesktopWidget>
#include <QAction>
#include <QIcon>
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>
#include <QPushButton>
#include <QFont>
#include <QTimer>
#include <QLabel>
#include <QPoint>
#include <QMouseEvent>
#include <QDebug>
#include <QDateTime>

#include <settingscmdline/settingscmdline.h>

#include <mylog/mylog.h>

#include "angleeditor.h"
#include "angleconverter.h"
#include "manualmovewindow.h"
#include "scdintfdevice.h"
#include "plot.h"
#include "motorencodercontrol.h"
#include "anglecontroller.h"
#include "PopupComon.h"

#include "mainform.h"

#define SECOND_PASS_FACTOR          5.0
#define THIRD_PASS_FACTOR           8.0

#define FIRST_PASS_ACURACY(x)       (x/1.5)
#define SECOND_PASS_ACURACY(x)      (x / 1.25)
#define THIRD_PASS_ACURACY(x)       (x)
#define THIRD_PASS_ACURACY_SLOW     false

#define REVERS_PASSES_ACURACY(x)    (x / 3.0)

Mainform::Mainform(MotorEncoderControl* mec, ScdIntfdevice* scd, QWidget *parent) :
    QMainWindow(parent)
{
    MEC = mec;
    IR2 = scd;
    currentStage = IDLE;

    resize(800, 600);
    // в центр монитора
    setGeometry(QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, size(),
                                    qApp->desktop()->availableGeometry()));

    plot = new Plot;
    setCentralWidget(plot);
    mmWin = new ManualMoveWindow(mec, this);
    updatetimer = new QTimer(this);
    updatetimer->setInterval(50);

    createMenus();
    createToolbars();
    connectSignalsAndSlots();

    CurrentPosToolbarLabel->installEventFilter(this);
    updatetimer->start();
}

bool Mainform::eventFilter(QObject *obj, QEvent *e)
{
    QMouseEvent *me = static_cast<QMouseEvent*>(e);
    PopupComon *popup = NULL;
    if (obj == CurrentPosToolbarLabel && (currentStage == IDLE || currentStage == MANUAL))
        switch(e->type())
        {
        case QEvent::MouseButtonPress:
            if (me->button() == Qt::LeftButton)
            {
                popup = new PopupComon();
                popup->lbl->setText(trUtf8("Угол назначения"));
                popup->btn->setIcon(QIcon(":/res/Forvard.png"));
                popup->btn->setToolTip(trUtf8("Старт"));
                connect(popup, SIGNAL(confirmed(AngleConverter)), MEC, SLOT(doAngle(AngleConverter)));
            }
            break;
        }

    if (popup)
    {
        popup->editor->setValue(AngleConverter(MEC->curentPos()));
        popup->move(me->globalPos());
        popup->show();
    }

    return QMainWindow::eventFilter(obj, e);
}

void Mainform::StartStop()
{
    StartStop(currentStage == IDLE);
}

void Mainform::StartStop(bool start)
{
    SetButton(start);
    if (start)
    {
        ResultLabel->setText(trUtf8("---"));
        if (manualWork->isChecked())
        {
            currentStage = MANUAL;
            IR2->Start();
        }
        else
        {
            MEC->doAngle(angleController->StartAngle());
            StopAction->setEnabled(false);
            ResetAction->setEnabled(false);
            currentStage = PRESTART;
        }
        plot->clear();
    }
    else
    {
        MEC->stop();
        IR2->Stop();
        StopAction->setEnabled(true);
        ResetAction->setEnabled(true);
        currentStage = IDLE;
    }
}

void Mainform::createMenus()
{
    IR2Setup = new QAction(QIcon(":/res/RadioActivCaution.png"), trUtf8("Настрока ИР-2"), this);
    MECSetup = new QAction(QIcon(":/res/Run.png"), trUtf8("Настрока привода"), this);
    MECSetup->setEnabled(false); //TODO
    manualWork = new QAction(QIcon(":/res/arm.PNG"), trUtf8("Ручной режим"), this);
    manualWork->setCheckable(true);

    QStringList l;
    l << trUtf8("Юстировка") << trUtf8("X-срез (18°)")
      << trUtf8("Y-срез (7°)") << trUtf8("Y-срез (10°)")
      << trUtf8("Z-срез (22°)") << trUtf8("Z-срез (25°)")
      << trUtf8("AT-срез (13°)") << trUtf8("AT-срез (10°)");
    AngleController::enCutType i = AngleController::UST;
    AngleController::enCutType selected =
            (AngleController::enCutType)SettingsCmdLine::settings->value("LastWorktype", AngleController::UST).toInt();
    if (selected > AngleController::VARIANTS_COUNT)
        selected = AngleController::UST;
    angleController = new AngleController(selected);
    foreach(const QString s, l)
    {
        QAction *a = new QAction(s, this);
        a->setCheckable(true);
        if (i == selected)
            a->setChecked(true);
        connect(a, SIGNAL(triggered()), this, SLOT(WorkTypeChanged()));
        WorkVariants.append(a);
        i = (AngleController::enCutType)(i + 1);
    }

    StopAction = new QAction(QIcon(":/res/Stop-Normal-Red-icon.png"), trUtf8("Стоп"), this);
    ResetAction = new QAction(QIcon(":/res/reset.png"), trUtf8("Сброс"), this);

    QMenu *pMenu = this->menuBar()->addMenu(trUtf8("&Настройка"));
    pMenu->addAction(IR2Setup);
    pMenu->addAction(MECSetup);
    pMenu->addSeparator();
    pMenu->addAction(StopAction);
    pMenu->addAction(ResetAction);

    pMenu = this->menuBar()->addMenu(trUtf8("&Режим"));
    pMenu->addAction(manualWork);

    pMenu = this->menuBar()->addMenu(trUtf8("&Вид работ"));
    pMenu->addActions(WorkVariants);

    int selected_stages = SettingsCmdLine::settings->value("LastStagesNum", 1).toInt();
    for (int i = 1; i < 4; ++i)
    {
        QAction* a = new QAction(QString::number(i), this);
        a->setCheckable(true);
        if (i == selected_stages)
            a->setChecked(true);
        connect(a, SIGNAL(triggered()), this, SLOT(StagesChanged()));
        stages.append(a);
    }
    pMenu = this->menuBar()->addMenu(trUtf8("&Количество проходов"));
    pMenu->addActions(stages);
}

void Mainform::createToolbars()
{
    // верхняя
    QToolBar *toolbar = new QToolBar(trUtf8("Настройки"));
    toolbar->addAction(IR2Setup);
    toolbar->addAction(MECSetup);
    toolbar->addSeparator();
    toolbar->addAction(manualWork);
    QWidget* w = new QWidget;
    w->setSizePolicy(QSizePolicy::Expanding,  QSizePolicy::Minimum);
    toolbar->addWidget(w);

    ResultLabel = new QLabel(trUtf8("---"));
    toolbar->addWidget(ResultLabel);

    toolbar->addAction(StopAction);
    toolbar->addAction(ResetAction);

    CurrentPosToolbarLabel = new QLabel;
    QFont fnt = CurrentPosToolbarLabel->font();
    fnt.setPointSize(18);
    fnt.setBold(true);
    CurrentPosToolbarLabel->setFont(fnt);
    CurrentPosToolbarLabel->setStyleSheet("color: #006F1A");
    CurrentPosToolbarLabel->setTextFormat(Qt::RichText);
    ResultLabel->setFont(fnt);
    toolbar->addWidget(CurrentPosToolbarLabel);
    this->addToolBar(Qt::TopToolBarArea, toolbar);

    //Нижняя
    startStopBtn = new QPushButton;
    SetButton(false);
    startStopBtn->setMinimumHeight(30);
    startStopBtn->setMinimumWidth(120);
    startStopBtn->setIconSize(QSize(20, 20));
    fnt = startStopBtn->font();
    fnt.setBold(true);
    startStopBtn->setFont(fnt);
    clearPlot = new QAction(QIcon(":/res/edit-clear_3678.png"), trUtf8("Очистить"), this);

    toolbar = new QToolBar(trUtf8("Управлеине"));
    toolbar->addAction(clearPlot);
    toolbar->addWidget(startStopBtn);
    this->addToolBar(Qt::BottomToolBarArea, toolbar);
}

void Mainform::connectSignalsAndSlots()
{
    connect(IR2Setup, SIGNAL(triggered()), IR2, SLOT(Setup()));
    connect(startStopBtn, SIGNAL(clicked()), this, SLOT(StartStop()));
    connect(clearPlot, SIGNAL(triggered()), plot, SLOT(clear()));
    connect(manualWork, SIGNAL(triggered(bool)), plot, SLOT(setTimeMode(bool)));
    //connect(manualWork, SIGNAL(toggled(bool)), mmWin, SLOT(setVisible(bool)));
    connect(mmWin, SIGNAL(rejected()), this, SLOT(disableManualControl()));
    connect(IR2, SIGNAL(data(ScdIntfdevice::Measure)), this, SLOT(measureResived(ScdIntfdevice::Measure)));
    connect(MEC, SIGNAL(MoveFinished()), this, SLOT(MoveFinished()));
    connect(StopAction, SIGNAL(triggered()), MEC, SLOT(stop()));
    connect(ResetAction, SIGNAL(triggered()), this, SLOT(GotoZero()));

    connect(updatetimer, SIGNAL(timeout()), this, SLOT(UpdatePosOnToolbar()));
    // Убрать потом
    //manualWork->setChecked(true);

    QAction *resetPosAction = new QAction(QIcon(":/res/edit-clear_3678.png"), trUtf8("Установить 0"), this);
    QAction *setPosAction = new QAction(QIcon(":/res/1390477646_play_green_controls.png"), trUtf8("Установить текущую позицию"), this);
    CurrentPosToolbarLabel->addAction(resetPosAction);
    CurrentPosToolbarLabel->addAction(setPosAction);
    CurrentPosToolbarLabel->setContextMenuPolicy(Qt::ActionsContextMenu);
    connect(resetPosAction, SIGNAL(triggered()), this, SLOT(ResetCurrentPos()));
    connect(setPosAction, SIGNAL(triggered()), this, SLOT(EditCurentPos()));

    plot->setTimeMode(manualWork->isChecked());
}

void Mainform::SetButton(bool startStop)
{
    if (startStop)
    {
        startStopBtn->setIcon(QIcon(":/res/Stop-Normal-Red-icon.png"));
        startStopBtn->setText(trUtf8("Стоп"));
    }
    else
    {
        startStopBtn->setIcon(QIcon(":/res/1390477646_play_green_controls.png"));
        startStopBtn->setText(trUtf8("Старт"));
    }
}

double Mainform::mesureStepAcurasy() const
{
    return SettingsCmdLine::settings->value("Mesurment/Acuracy").toDouble();
}

static QColor colorgenerator()
{
    return QColor(Qt::blue);
}

void Mainform::measureResived(ScdIntfdevice::Measure m)
{
    switch (currentStage)
    {
    case MANUAL:
        plot->addPoint(QDateTime::currentMSecsSinceEpoch(), m.pulseNo);
        break;
    case FORVARD_QUICK:
    case FORVARD_NORMAL:
    case FORVARD_SLOW:
        plot->addPoint(MEC->curentPos(), m.pulseNo);
        break;
    default:
        return;
    }
}

void Mainform::disableManualControl()
{
    manualWork->setChecked(false);
}

void Mainform::MoveFinished()
{
    double FoundMaxPos;
    switch(currentStage)
    {
    case PRESTART: // доехали до начальной точки
        plot->startNewCurve(Qt::red);
        MEC->doAngle(angleController->StartAngle() + angleController->SearchArc(),
                     FIRST_PASS_ACURACY(mesureStepAcurasy()));
        IR2->Start();
        break;
    case FORVARD_QUICK: // проехали превый проход, возврат
        FoundMaxPos = plot->BuildSmoothCurve();
        if (stages.at(0)->isChecked())
            goto stop_m;
        if (!std::isnan(FoundMaxPos) /*&&
                (FoundMaxPos > angleController->StartAngle()) &&
                (FoundMaxPos < angleController->EndAngle())*/
                )
            MEC->doAngle(FoundMaxPos - angleController->SearchArc() / SECOND_PASS_FACTOR,
                         REVERS_PASSES_ACURACY(mesureStepAcurasy()));
        else
            MEC->doAngle(angleController->StartAngle(),
                         REVERS_PASSES_ACURACY(mesureStepAcurasy()));
        break;
    case BACK1: // Вернулись
        FoundMaxPos = plot->lastMaximum();
        plot->startNewCurve(Qt::magenta);
        if (!std::isnan(FoundMaxPos) /*&&
                        (FoundMaxPos > angleController->StartAngle()) &&
                        (FoundMaxPos < angleController->EndAngle())*/
            )
            MEC->doAngle(FoundMaxPos + angleController->SearchArc() / SECOND_PASS_FACTOR,
                         SECOND_PASS_ACURACY(mesureStepAcurasy()));
        else
            MEC->doAngle(angleController->StartAngle() + angleController->SearchArc(),
                         SECOND_PASS_ACURACY(mesureStepAcurasy()));
        break;
    case FORVARD_NORMAL: // проехали второй проход, возврат
        FoundMaxPos = plot->BuildSmoothCurve();
        if (stages.at(1)->isChecked())
            goto stop_m;
        if (!std::isnan(FoundMaxPos) /*&&
                        (FoundMaxPos > angleController->StartAngle()) &&
                        (FoundMaxPos < angleController->EndAngle())*/
            )
            MEC->doAngle(FoundMaxPos - angleController->SearchArc() / THIRD_PASS_FACTOR,
                         REVERS_PASSES_ACURACY(mesureStepAcurasy()));
        else
            MEC->doAngle(angleController->StartAngle(),
                         REVERS_PASSES_ACURACY(mesureStepAcurasy()));
        break;
    case BACK2: // проехали возвратный проход
        FoundMaxPos = plot->lastMaximum();
        plot->startNewCurve(Qt::blue);
        if (!std::isnan(FoundMaxPos) /*&&
                        (FoundMaxPos > angleController->StartAngle()) &&
                        (FoundMaxPos < angleController->EndAngle())*/
            )
            MEC->doAngle(FoundMaxPos + angleController->SearchArc() / THIRD_PASS_FACTOR,
                         THIRD_PASS_ACURACY(mesureStepAcurasy()), THIRD_PASS_ACURACY_SLOW);
        else
            MEC->doAngle(angleController->StartAngle() + angleController->SearchArc(),
                         THIRD_PASS_ACURACY(mesureStepAcurasy()), THIRD_PASS_ACURACY_SLOW);
        break;
    case FORVARD_SLOW: // Готово
        FoundMaxPos = plot->BuildSmoothCurve();
        ResultLabel->setText(angleController->convertToCutRes(FoundMaxPos));
        StartStop(false);
        currentStage = IDLE;
        // fixing pass error
        MEC->SetCurentPos(MEC->curentPos() + angleController->getPassError());
        LOG_INFO(trUtf8("Pass error correction: %1").arg(
                     AngleConverter(angleController->getPassError()).toString()));
        return;

    default:
        return;
    }

    currentStage = (stage)(currentStage + 1);
    return;

stop_m:
    StartStop(false);
    ResultLabel->setText(angleController->convertToCutRes(FoundMaxPos));
    return;
}

void Mainform::WorkTypeChanged()
{
    QAction *Sender = (QAction*)sender();
    AngleController::enCutType i = AngleController::UST;
    AngleController::enCutType clicked;
    foreach(QAction* a, WorkVariants)
    {
        if (a == Sender)
        {
            a->setChecked(true);
            clicked = i;
        }
        else
            a->setChecked(false);

        i = (AngleController::enCutType)(i + 1);
    }

    (*SettingsCmdLine::settings)["LastWorktype"] = clicked; // save work type

    if (angleController != NULL)
        delete angleController;
    angleController = new AngleController(clicked);
}

void Mainform::UpdatePosOnToolbar()
{
    CurrentPosToolbarLabel->setText(AngleConverter(MEC->curentPos()).toString());
}

void Mainform::GotoZero()
{
    MEC->doAngle(0);
}

void Mainform::ResetCurrentPos()
{
    MEC->SetCurentPos(0.0);
}

void Mainform::EditCurentPos()
{
    PopupComon *popup = new PopupComon();
    popup->lbl->setText(trUtf8("Текущий угол"));
    popup->btn->setIcon(QIcon(":/res/Forvard.png"));
    popup->btn->setToolTip(trUtf8("Задать"));
    popup->editor->setValue(AngleConverter(MEC->curentPos()));
    popup->move(QCursor::pos());
    popup->show();

    connect(popup, SIGNAL(confirmed(AngleConverter)), MEC, SLOT(SetCurentPos(AngleConverter)));
}

void Mainform::StagesChanged()
{
    QAction *Sender = (QAction*)sender();
    int i = 1;
    int clicked;
    foreach(QAction* a, stages)
    {
        if (a == Sender)
        {
            a->setChecked(true);
            clicked = i;
        }
        else
            a->setChecked(false);

        ++i;
    }

    (*SettingsCmdLine::settings)["LastStagesNum"] = clicked; // save work type
}
