#include <QStringList>

#include "mesureangletorealanglesolver.h"

MesureAngleToRealAngleSolver::MesureAngleToRealAngleSolver()
{
    reset();
}

double MesureAngleToRealAngleSolver::solve(double input)
{
    return coeffs["k"] * input + coeffs["b"];
}

bool MesureAngleToRealAngleSolver::setCoeffs(const QMap<QString, double> &coeffs)
{
    QStringList keys = coeffs.keys();
    if (keys.contains("k") &&
            keys.contains("b"))
    {
        this->coeffs = coeffs;
        return true;
    }
    return false;
}

void MesureAngleToRealAngleSolver::reset()
{
    coeffs["b"] = 0;
    coeffs["k"] = 1;
}
