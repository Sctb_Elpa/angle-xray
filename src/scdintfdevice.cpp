#include <strings.h>


#include <mylog/mylog.h>

#include "ir2setupwindow.h"

#include "scdintfdevice.h"

QMap<struct Scd*, ScdIntfdevice*> ScdIntfdevice::mapScd2class;

static const char* const errstr[] =
{
    "Успешное завершение",
    "Переданный хэндл (handle) не является хэндлом детектора.",
    "Значение порога вне диапазона 0..1023",
    "Нижний порог больше верхнего.",
    "Значение числа импульсов вне диапазона 1..1000000000",
    "Значение напряжения вне диапазона 0..818 Вольт",
    "Значение времени экспозиции вне диапазона 0.001.. 1000 сек.",
    "Константа времени для интенсивности вне диапазона 0.01..1000 сек",
    "Не удаётся открыть указанный COM порт (он не существует или уже занят)."
};

enum
{
    TIME_CONST = 0,
    LOW_T = 1,
    HI_T = 2,
    INTEGRAL_MODE = 3,
    VOLTAGE = 4
};

static const QString ir2prefix("IR-2/");

static const char* const settingsNames[] =
{
    "TimeConst", "WindowLowThreshold", "WindowHighThreshold", "IntegralMode", "Voltage"
};

ScdIntfdevice::ScdIntfdevice(int portNum, QObject *parent) :
    QObject(parent)
{
    errCode err = ScdConnect(portNum, &thisScd) ;
    if (err != eScdDllOk)
    {
        thisScd = NULL;
        LOG_ERROR(trUtf8("Faled to connect to IR-2 by COM%1").arg(portNum));
        isOk = false;
        return;
    }
    else
    {
        // test
        /* Точного теста нет, сори.
        double t;
        err = GetExposureTime(thisScd, &t);
        if ((err != eScdDllOk) || (t == 0.0))
        {
            ScdDisconnect(thisScd);
            thisScd = NULL;
            LOG_ERROR(trUtf8("Faled to connect to IR-2 by COM%1").arg(portNum));
            isOk = false;
            return;
        }
        */
        isOk = true;
    }
    Stop(); // Профилактика, возможно измерение еще шло до запуска проги.
    mapScd2class.insert(thisScd, this);
    /*CALLBCKS*/
    RegisterAcqStartProc(thisScd, MeasureStartCallback);
    RegisterAcqStopProc(thisScd, MeasureStopCallback);
    RegisterAcqDataProc(thisScd, DataProcCallback);

    ResetCard(thisScd);

    reloadSettings();
}

ScdIntfdevice::~ScdIntfdevice()
{
    if (thisScd)
    {
        //save settings
        readCfg();

        errCode err = ScdDisconnect(thisScd);
        if (err != eScdDllOk)
            LOG_ERROR(trUtf8("Error %1 while disconnecting IR-2").arg(err));
    }
}


QString ScdIntfdevice::err2sttr(errCode code) const
{
    return trUtf8(errstr[static_cast<int>(code)]);
}

void ScdIntfdevice::setTimeConst(double timeconst)
{
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[TIME_CONST]] = timeconst;
    reloadSettings();
}

void ScdIntfdevice::setVoltage(int voltage)
{
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[VOLTAGE]] = voltage;
    reloadSettings();
}

void ScdIntfdevice::setLowerThreshold(int value)
{
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[HI_T]] = value;
    reloadSettings();
}

void ScdIntfdevice::setUpperThreshold(int value)
{
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[LOW_T]] = value;
    reloadSettings();
}

void ScdIntfdevice::setIntegralMode(bool enable)
{
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[INTEGRAL_MODE]] = enable;
    reloadSettings();
}

void ScdIntfdevice::setSoundMode(bool enableSound)
{
    SetSoundMode(thisScd, enableSound);
}

void ScdIntfdevice::reset()
{
    ResetCard(thisScd);
    readCfg();
}

void ScdIntfdevice::Setup()
{
    IR2SetupWindow setupdialog;
    // запись
    setupdialog.setTimeConst(SettingsCmdLine::settings->value(ir2prefix + settingsNames[TIME_CONST], 1).toDouble());
    setupdialog.setWindowLowThreshold(SettingsCmdLine::settings->value(ir2prefix + settingsNames[LOW_T], 0).toUInt());
    setupdialog.setWindowHighThreshold(SettingsCmdLine::settings->value(ir2prefix + settingsNames[HI_T], 1).toUInt());
    setupdialog.setIntegralMode(SettingsCmdLine::settings->value(ir2prefix + settingsNames[INTEGRAL_MODE], false).toBool());
    setupdialog.setVoltage(SettingsCmdLine::settings->value(ir2prefix + settingsNames[VOLTAGE], 0).toUInt());
    if (setupdialog.exec() == QDialog::Accepted)
    {
        //чтение
        (*SettingsCmdLine::settings)[ir2prefix + settingsNames[TIME_CONST]] = setupdialog.getTimeConst();
        (*SettingsCmdLine::settings)[ir2prefix + settingsNames[LOW_T]] = setupdialog.getWindowLowThreshold();
        (*SettingsCmdLine::settings)[ir2prefix + settingsNames[HI_T]] = setupdialog.getWindowHighThreshold();
        (*SettingsCmdLine::settings)[ir2prefix + settingsNames[INTEGRAL_MODE]] = setupdialog.isIntegralMode();
        (*SettingsCmdLine::settings)[ir2prefix + settingsNames[VOLTAGE]] = setupdialog.getVoltage();
        reloadSettings();
    }
}

void ScdIntfdevice::Start()
{
    StartMeasurement(thisScd);
}

void ScdIntfdevice::Stop()
{
    StopMeasurement(thisScd);
}

void ScdIntfdevice::reloadSettings()
{
    errCode res;
    res = SetMode(thisScd, smIntensity);
    res = SetTimeConst(thisScd, SettingsCmdLine::settings->value(ir2prefix + settingsNames[TIME_CONST], 1).toDouble());
    res = SetVoltage(thisScd, SettingsCmdLine::settings->value(ir2prefix + settingsNames[VOLTAGE], 0).toInt());
    res = SetLowerThreshold(thisScd, SettingsCmdLine::settings->value(ir2prefix + settingsNames[LOW_T], 0).toInt());
    res = SetUpperThreshold(thisScd, SettingsCmdLine::settings->value(ir2prefix + settingsNames[HI_T], 1).toInt());
    res = SetIntegralMode(thisScd, SettingsCmdLine::settings->value(ir2prefix + settingsNames[INTEGRAL_MODE], false).toBool());
    LOG_DEBUG(trUtf8("IR2 reloaded settings, res = %1").arg(res));
}

void ScdIntfdevice::readCfg()
{
    double d;
    int i;
    bool b;

    GetIntegralMode(thisScd, &b);
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[INTEGRAL_MODE]] = b;
    GetVoltage(thisScd, &i);
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[VOLTAGE]] = i;
    GetUpperThreshold(thisScd, &i);
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[HI_T]] = i;
    GetLowerThreshold(thisScd, &i);
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[LOW_T]] = i;
    GetTimeConst(thisScd, &d);
    (*SettingsCmdLine::settings)[ir2prefix + settingsNames[TIME_CONST]] = d;
}

void ScdIntfdevice::MeasureStartCallback(Scd *ScdHandle)
{
    ScdIntfdevice* _this = mapScd2class.value(ScdHandle);
    LOG_DEBUG(trUtf8("Measure started (instatce 0x%1)").arg((size_t)_this, 0, 16));
    emit _this->started();
}

void ScdIntfdevice::MeasureStopCallback(Scd *ScdHandle)
{
    ScdIntfdevice* _this = mapScd2class.value(ScdHandle);
    LOG_DEBUG(trUtf8("Measure stoped (instatce 0x%1)").arg((size_t)_this, 0, 16));
    emit _this->stopped();
}

void ScdIntfdevice::DataProcCallback(Scd *ScdHandle, double time, int PulseNo)
{
    ScdIntfdevice* _this = mapScd2class.value(ScdHandle);
    //LOG_DEBUG(trUtf8("data ressived: time=%2,\tPulseNo=%3 (instatce 0x%1)").arg((size_t)_this, 0, 16).arg(time).arg(PulseNo));
    Measure m = {time, PulseNo};
    emit _this->data(m);
}
