#include <QStringList>

#include "atresultconverter.h"

AtResultConverter::AtResultConverter()
{
    reset();
}

double AtResultConverter::solve(double input)
{
    return coeffs["A1"] - (coeffs["A2"] - input);
}

void AtResultConverter::reset()
{
    coeffs["A1"] = 0;
    coeffs["A2"] = 0;
}

QMap<QString, double> AtResultConverter::getCoeffs() const
{
    return coeffs;
}

bool AtResultConverter::setCoeffs(const QMap<QString, double> &coeffs)
{
    QStringList keys = coeffs.keys();
    if (keys.contains("A1") &&
            keys.contains("A2"))
    {
        this->coeffs = coeffs;
        return true;
    }
    return false;
}
