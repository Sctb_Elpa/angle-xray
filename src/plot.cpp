#include <qwt/qwt_symbol.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_plot_marker.h>
#include <qwt/qwt_scale_engine.h>

#include <QPalette>
#include <QAction>
#include <QFile>
#include <QByteArray>
#include <QFileDialog>
#include <QDir>
#include <QStringList>
#include <QtAlgorithms>

#include <math.h>

#include <mylog/mylog.h>

#include <smoothspline/SmoothSpline.h>

#include "stocktimescaledraw.h"
#include "stockAnglescaledraw.h"
#include "angleconverter.h"

#include "plot.h"

#define SEARCH_STEP             (1.0 / 60 / 60)

//#define SPLINE_SMOOTH_FACTOR    (1e-8)
#define SPLINE_SMOOTH_FACTOR    (0.0000005)

Plot::Plot(QWidget *parent) :
    QwtPlot(parent)
{
    currentcurve = NULL;
    enableAxis(QwtPlot::yLeft, false);
    setAutoFillBackground(true);
    QPalette palete = palette();
    palete.setColor(QPalette::Window, QColor(Qt::white));
    setPalette(palete);

    QAction *ASaveData = new QAction(QIcon(":/res/saveData.png"), trUtf8("Сохранить данные в файл"), this);
    connect(ASaveData, SIGNAL(triggered()), this, SLOT(SaveData()));
    addAction(ASaveData);

    setContextMenuPolicy(Qt::ActionsContextMenu);
    //axisScaleEngine(QwtPlot::yLeft)->setMargins(0, 0);
}

Plot *Plot::fromData(const QByteArray &d)
{
    Plot* p = new Plot();
    p->setTimeMode(false);

    QList<QColor> colorlist;
    colorlist << Qt::red << Qt::magenta << Qt::blue << Qt::green;

    QRegExp checker_data("([\\d,.]*);(\\d*)");
    QRegExp checker_curves("Curve 0x[\\D,\\d]*:");
    QStringList curvesdata = QString(d).split("\n");
    int i = 0;
    QVector<double> X, Y;
    foreach(QString s, curvesdata)
    {
        if (checker_data.exactMatch(s))
        {
            checker_data.indexIn(s);
            QStringList vals = checker_data.capturedTexts();
            X.append(vals.at(1).toDouble());
            Y.append(vals.at(2).toDouble());
        }
        else if (checker_curves.exactMatch(s))
        {
            if (!X.isEmpty())
                p->SetData(X, Y);
            p->BuildSmoothCurve();
            p->startNewCurve(colorlist.at(i++));

            X.clear();
            Y.clear();
        }
    }
    if (!X.isEmpty())
    {
        p->SetData(X, Y);
        p->BuildSmoothCurve();
    }

    return p;
}

QVector<QPointF> Plot::buildSmoothData(const QVector<double> &srcX, const QVector<double> &srcY,
                                       QPointF * max)
{
    spline_range *spline;
    QVector<double> X, Y;

    if(srcX.first() > srcX.last())
    {
        // вывернуть
        int l = srcX.size();
        X.resize(l);
        Y.resize(l);
        for(int i = 0; i < l; ++i)
        {
            X[i] = srcX[l - i - 1];
            Y[i] = srcY[l - i - 1];
        }
    }
    else
    {
        X = srcX;
        Y = srcY;
    }
/*
    X.remove(0);
    X.remove(X.size() - 1);
    Y.remove(0);
    Y.remove(X.size() - 1);
*/
    SplineApprox(X.data(), Y.data(), X.size(), SPLINE_SMOOTH_FACTOR, &spline);

    if (isnan(spline->A))
    {
        max->setX(NAN);
        max->setY(NAN);
        return QVector<QPointF>();
    }

    QVector<QPointF> res;
    double MaxX, maxVal = -INFINITY;

    for(int i = 0; i < X.size(); ++i)
    {
        double x = X[i];
        double y = calculateSplineAt(spline, X.size(), x).Y;
        if (y >= 0)
            res.append(QPointF(x, y));

        if (y > maxVal)
        {
            maxVal = y;
            MaxX = x;
        }
    }

    if (max)
    {
        double pos = MaxX;
        double d = calculateSplineAt(spline, X.size(), pos).d1Y;
        if (d > 0)
        {
            //   /-*-\
            // -+-----+-
            // /       \

            double db = calculateSplineAt(spline, X.size(), pos - SEARCH_STEP).d1Y;
            double da = calculateSplineAt(spline, X.size(), pos + SEARCH_STEP).d1Y;

            if (db > da)
            {
                do
                {
                    db = da;
                    da = calculateSplineAt(spline, X.size(), pos += SEARCH_STEP).d1Y;
                } while (db > 0 && da > 0);
                pos -= SEARCH_STEP / 2.0;
            }
            else
            {
                do
                {
                    da = db;
                    db = calculateSplineAt(spline, X.size(), pos -= SEARCH_STEP).d1Y;
                } while (db > 0 && da > 0);
                pos += SEARCH_STEP / 2.0;
            }
        }
        else
        {
            // \       /
            // -+-----+-
            //   \-*-/

            double db = calculateSplineAt(spline, X.size(), pos - SEARCH_STEP).d1Y;
            double da = calculateSplineAt(spline, X.size(), pos + SEARCH_STEP).d1Y;

            if (db > da)
            {
                do
                {
                    db = da;
                    da = calculateSplineAt(spline, X.size(), pos -= SEARCH_STEP).d1Y;
                } while (db < 0 && da < 0);
                pos += SEARCH_STEP / 2.0;
            }
            else
            {
                do
                {
                    da = db;
                    db = calculateSplineAt(spline, X.size(), pos += SEARCH_STEP).d1Y;
                } while (db < 0 && da < 0);
                pos -= SEARCH_STEP / 2.0;
            }
        }

        max->setX(pos);
        max->setY(calculateSplineAt(spline, X.size(), pos).Y);
    }

    return res;
}

double Plot::BuildSmoothCurve(QwtPlotCurve* curve)
{
    double res = NAN;
    if (!curve)
        curve = currentcurve;
    if (curve && curve->data()->size())
    {
        QPointF maximum;
        QVector<double> dataX, dataY;
        //Обратное действие, что поделать (отбросить точки с одинаковым X
        double lX = -1;
        for (int i = 0; i < curve->data()->size(); ++i)
        {
            double x = curve->sample(i).x();

            if (fabs(lX - x) < 0.1/3600.0)
            {
                //lX = x;
                continue;
            }
            else
                lX = x;

            dataX.append(x);
            dataY.append(curve->sample(i).y());
        }

        LOG_DEBUG(trUtf8("Data size: %1:%2").arg(dataX.size()).arg(dataY.size()));

        QVector<QPointF> smoothPoints = buildSmoothData(dataX, dataY, &maximum);
        if (isnan(maximum.x()))
        {
            LOG_ERROR("Failed to calcule smooth spline");
            return LastMaximum;
        }
        LOG_DEBUG(trUtf8("Found maximum at: %1:%2").arg(maximum.x()).arg(maximum.y()));
        res = maximum.x();

        QwtSymbol * s = (QwtSymbol *)currentcurve->symbol();
        QPen p = currentcurve->pen();
        QColor c = p.color();

        QwtPlotCurve * smoothCurve = builCurve(c, QwtSymbol::NoSymbol, 2);
        smoothCurve->setSamples(smoothPoints);
        plotItems.append(smoothCurve);

        QwtPlotMarker *label = new QwtPlotMarker;
        label->setLabel(AngleConverter(maximum.x()).toString());
        label->setValue(maximum.x(), 0);
        label->setLabelAlignment(Qt::AlignLeft | Qt::AlignBottom);
        label->setLineStyle(QwtPlotMarker::VLine);
        label->setLinePen(p);
        label->setLabelOrientation(Qt::Vertical);
        label->attach(this);
        plotItems.append(label);

        c.setAlpha(60);
        p.setColor(c);
        s->setPen(p);
        currentcurve->setPen(p);

        replot();
    }
    else
        LOG_ERROR(trUtf8("Curve is empty nothing found"));

    return LastMaximum = res;
}

QwtPlotCurve *Plot::getCurrentcurve() const
{
    return currentcurve;
}

double Plot::lastMaximum() const
{
    return LastMaximum;
}

void Plot::startNewCurve(const QColor &color)
{
    dataX.clear();
    dataY.clear();
    currentcurve = builCurve(color, QwtSymbol::Ellipse, 2);
    raw.append(currentcurve);
}

void Plot::addPoint(double X, int Y)
{
    if (!currentcurve)
        startNewCurve();
    dataX.append(X);
    dataY.append(Y);
    currentcurve->setSamples(dataX, dataY);

    replot();
}

void Plot::SetData(const QVector<double> &X, const QVector<double> &Y)
{
    if (!currentcurve)
        startNewCurve();
    dataX = X;
    dataY = Y;
    currentcurve->setSamples(dataX, dataY);

    replot();
}

void Plot::setTimeMode(bool TimeMode)
{
    clear();
    if (TimeMode)
        this->setAxisScaleDraw(QwtPlot::xBottom, new StockTimeScaleDraw(trUtf8("mm.ss")));
    else
        this->setAxisScaleDraw(QwtPlot::xBottom, new StockAngleScaleDraw());
}

void Plot::clear()
{
    qDeleteAll(plotItems);
    qDeleteAll(raw);
    currentcurve = NULL;
    plotItems.clear();
    raw.clear();
    replot();
}

void Plot::SaveData()
{
    QString filename = QFileDialog::getSaveFileName(this, trUtf8("Сохранить данные как..."),
                                                    QDir::currentPath(), trUtf8("CSV table (*.csv)")
                                                #ifndef WIN32
                                                    , NULL, QFileDialog::DontUseNativeDialog
                                                #endif
                                                    );
    if (filename.isEmpty())
        return;
    if (!filename.endsWith(trUtf8(".csv")))
        filename.append(trUtf8(".csv"));
    QFile dataFile(filename);
    QByteArray t;
    if (!dataFile.open(QIODevice::WriteOnly))
    {
        LOG_ERROR(trUtf8("Failed to open file %1").arg(dataFile.fileName()));
        return;
    }


    foreach(const QwtPlotCurve* curve, raw)
    {
        const QwtSeriesData<QPointF> *d = curve->data();
        if(d->size() == 0)
            continue;

        dataFile.write(trUtf8("Curve 0x%1:\nX;Y\n").arg((size_t)curve, 0, 16).toLatin1());

        for (int i = 0; i < d->size(); ++i)
        {
            QPointF p = d->sample(i);
            dataFile.write(trUtf8("%1;%2\n").arg(p.x()).arg(p.y()).toLatin1());
        }

        dataFile.write("\n");
    }

    dataFile.close();
}

QwtPlotCurve *Plot::builCurve(const QColor &color, const QwtSymbol::Style symbolStyle, int penWidth)
{
    QwtSymbol *symbol = new QwtSymbol;
    QPen pen(color);
    pen.setWidth(penWidth);
    symbol->setStyle(symbolStyle);
    symbol->setPen(pen);
    symbol->setSize(penWidth);
    symbol->setBrush(QBrush(Qt::SolidPattern));

    QwtPlotCurve *curve = new QwtPlotCurve;
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    curve->setPen(pen);
    curve->setSymbol(symbol);
    curve->setAxes(QwtPlot::xBottom, QwtPlot::yLeft);
    curve->attach(this);

    return curve;
}

QwtPlotCurve *Plot::buildVertLine(const QColor &color, const QPointF &pos)
{
    QwtPlotCurve* res = builCurve(color, QwtSymbol::NoSymbol, 1);
    QVector<QPointF> d;
    d.append(QPointF(pos.x(), 0));
    d.append(QPointF(pos.x(), pos.y()));
    res->setSamples(d);

    return res;
}
