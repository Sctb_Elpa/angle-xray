#include <math.h>

#include <modbus/modbus-rtu.h>

#include <qmodbus/ModbusEngine.h>
#include <qmodbus/ModbusRequest.h>

#include <settingscmdline/settingscmdline.h>

#include <mylog/mylog.h>

#include "angleconverter.h"

#include "motorencodercontrol.h"

MotorEncoderControl::MotorEncoderControl(const QString port, QObject *parent) :
    QThread(parent)
{
    curentPosition = SettingsCmdLine::settings->value("MotorEncoder/LastPos", 0).toDouble();

    (*SettingsCmdLine::settings)["Global/Port"] = port;
    modbus = new ModbusEngine;
    modbus->RestartConnection();
    modbus->setTimeout(SettingsCmdLine::settings->value("Global/Timeout").toInt());

    WormGearRatio = SettingsCmdLine::settings->value("MotorEncoder/WormGearRatio", DEFAULT_WORM_GEAR_RATIO).toDouble();
    BeltDriveRatio = SettingsCmdLine::settings->value("MotorEncoder/BeltDriveRatio", DEFAULT_BELT_DRIVE_RATIO).toDouble();

    MotorResolution = 0;

    lastSide = SettingsCmdLine::settings->value("MotorEncoder/LastMoveSide", true).toBool();;
}

MotorEncoderControl::~MotorEncoderControl()
{
    if (isRunning())
        stop();
    delete modbus;

    (*SettingsCmdLine::settings)["MotorEncoder/LastPos"] = curentPos();
    (*SettingsCmdLine::settings)["MotorEncoder/LastMoveSide"] = lastSide;
}

bool MotorEncoderControl::test()
{
    bool r;
    ModbusRequest* testrequest = ModbusRequest::BuildReadHoldingRequest(ES_D508_MODBUS_ADRESS,
                                                                        0x00ff);
    modbus->SyncRequest(*testrequest);
    if (testrequest->getErrorrCode() != ModbusRequest::ERR_OK)
    {
        LOG_ERROR("TestRequest failed!");
        r = false;
    }
    else
    {
        QString res = testrequest->getAnsver().toHex();
        r = !res.isEmpty();
    }

    delete testrequest;
    return r;
}

double MotorEncoderControl::curentPos() const
{
    double res;
    mutex.lock();
    res = curentPosition;
    mutex.unlock();
    return res;
}

int MotorEncoderControl::getFreeMoveRes()
{
    int r = SettingsCmdLine::settings->value("Mesurment/FreeMoveAcuracy", ES_D508_RESOLUTION_MIN).toInt();
    return r < ES_D508_RESOLUTION_MIN ? ES_D508_RESOLUTION_MIN : r;
}

double MotorEncoderControl::setResolution(double ticPreDegree) // тиков на реальный градус
{
    // в реальном градусе оборотов
    double RoundsPreRealDegree = trnasmissionRatio();

    // на RoundsPreRealDegree оборотов дается ticPreDegree тиков, значит на 1 оборот:
    uint16_t d = lround(ticPreDegree / RoundsPreRealDegree);

    if (d < ES_D508_RESOLUTION_MIN)
        d = ES_D508_RESOLUTION_MIN;
    if (d > ES_D508_RESOLUTION_MAX)
        d = ES_D508_RESOLUTION_MAX;

    ModbusRequest *req1 = ModbusRequest::BuildWriteHoldingRequest(ES_D508_MODBUS_ADRESS,
                                                                  ES_D508_RESOLUTION_ADDR,
                                                                  QByteArray((char*)&d, sizeof(uint16_t)));

    /*   ModbusRequest *req2 = ModbusRequest::BuildWriteHoldingRequest(ES_D508_MODBUS_ADRESS,
                                                                  ES_D508_SENSOR_RESOLUTION_ADDR,
                                                                  QByteArray((char*)&d, sizeof(uint16_t)));
                                                                  */
    modbus->SyncRequest(*req1);
    // modbus->SyncRequest(*req2);
    if (
            req1->getErrorrCode() != ModbusRequest::ERR_OK /*||
                                            req2->getErrorrCode() != ModbusRequest::ERR_OK*/
            )
    {
        LOG_ERROR(trUtf8("Failed to set resolution (%1 && %2)").arg(req1->getErrorrCode())/*.arg(req2->getErrorrCode())*/);
        d = -1;
    }
    delete req1;
    // delete req2;

    MotorResolution = d * RoundsPreRealDegree;

    LOG_DEBUG(trUtf8("Resolution set to %1*%2 [st./°]").arg(d).arg(RoundsPreRealDegree));

    return MotorResolution;
}

double MotorEncoderControl::resolution() const
{
    // в реальном градусе оборотов
    double RoundsPreRealDegree = trnasmissionRatio();

    uint16_t result;

    ModbusRequest *req = ModbusRequest::BuildReadHoldingRequest(ES_D508_MODBUS_ADRESS,
                                                                ES_D508_RESOLUTION_ADDR);
    modbus->SyncRequest(*req);
    if (req->getErrorrCode() != ModbusRequest::ERR_OK)
    {
        LOG_ERROR("Failed to set resolution");
        result = -1;
    }
    else
        result = req->getAnsverAsShort().at(0);
    delete req;

    return result * RoundsPreRealDegree; // тиков на 1 реальный градус
}

void MotorEncoderControl::SetCurentPos(double Angle)
{
    mutex.lock();
    curentPosition = Angle;
    mutex.unlock();
    LOG_INFO(trUtf8("Current position changed to %1").arg(Angle));
    emit AngleChanged(Angle);
}

void MotorEncoderControl::SetCurentPos(const AngleConverter &Angle)
{
    SetCurentPos(Angle.getAngle());
}

void MotorEncoderControl::setWormGearRatio(double AnglePreTurn)
{
    (*SettingsCmdLine::settings)["MotorEncoder/WormGearRatio"] = WormGearRatio = AnglePreTurn;
}

void MotorEncoderControl::setBeltDriveRatio(double ratio)
{
    (*SettingsCmdLine::settings)["MotorEncoder/BeltDriveRatio"] = BeltDriveRatio = ratio;
}

void MotorEncoderControl::stop()
{
    mutex.lock();
    fStop = true;
    mutex.unlock();

    while(isRunning()); // wait for termination
}

double MotorEncoderControl::trnasmissionRatio() const
{
    // мотор -> Ремень -> червяк
    // 1 градус поворота мотора соответствует x градусов поворота образца
    return BeltDriveRatio * WormGearRatio;
    // ремень        //Червяк
}

double MotorEncoderControl::motorHysteresis() const
{
    return AngleConverter::parce(
                SettingsCmdLine::settings->value("Global/MotorHisterezis").toString()).getAngle();
}

#if USE_FTDI_DRIVER == 1
void MotorEncoderControl::controlPrepare()
{
    modbus->close(); // 2 либы не могут одновременно открывать FT-шку
    int res = FT_Open(0, &ftHandle);
    if (res != FT_OK)
    {
        LOG_ERROR(trUtf8("FT_Open() failed! (%1)").arg(res));
        ftHandle = NULL;
    }
}

void MotorEncoderControl::controlFinish()
{
    int res = FT_Close(ftHandle);
    if (res != FT_OK)
        LOG_ERROR(trUtf8("FT_Close() failed! (%1)").arg(res));
    else
        ftHandle = NULL;
}

void MotorEncoderControl::setStep(bool val)
{
    if (ftHandle)
    {
        if (val)
            FT_SetDtr(ftHandle);
        else
            FT_ClrDtr(ftHandle);
    }
}

void MotorEncoderControl::setDir(bool val)
{
    if (ftHandle)
    {
        if (val)
            FT_SetRts(ftHandle);
        else
            FT_ClrRts(ftHandle);
    }
}

#else
void MotorEncoderControl::controlPrepare()
{
    ctx = modbus->getModbusContext();
    //modbus_connect(ctx);
}

void MotorEncoderControl::controlFinish()
{
}

void MotorEncoderControl::setStep(bool val)
{
    _modbus_rtu_ioctl_dtr(ctx, (int)val);
}

void MotorEncoderControl::setDir(bool val)
{
    _modbus_rtu_ioctl_rts(ctx, (int)val);
}
#endif

void MotorEncoderControl::run()
{
    mutex.lock();
    fStop = false;
    mutex.unlock();

    for (;;)
    {
        mutex.lock();
        if (fStop)
        {
            mutex.unlock();
            return;
        }
        if (queue.isEmpty())
        {
            mutex.unlock();
            msleep(1);
        }
        else
        {
            bool verySlowMode;
            moveCommand cmd = queue.dequeue();
            // нужно проехать
            verySlowMode = cmd.forceSlow;
            double toGo = cmd.destination - curentPosition;
            mutex.unlock();

            LOG_DEBUG(trUtf8("To go: %1").arg(AngleConverter(toGo).toString()));

            if ((toGo > 0 && !lastSide) ||
                    (toGo < 0 && lastSide))
            {
                if (lastSide)
                    toGo -= motorHysteresis();
                else
                    toGo += motorHysteresis();
            }

            if (fabs(toGo) < (0.1 / 3600.0)) // если ехать меньше 0.1-секунды
            {
                emit AngleChanged(curentPosition);
                emit MoveFinished();
                continue;
            }

            if (cmd.minResolution < RESOLUTION_MIN)
                cmd.minResolution = RESOLUTION_MIN;

            if (cmd.minResolution > RESOLUTION_MAX)
            {
                verySlowMode = true;
                cmd.minResolution = RESOLUTION_MAX;
            }


            // сколько шагов нужно сделать, чтобы проехать toGo с разрешением cmd.minResolution
            int stepCount = lround(toGo * cmd.minResolution);
            // цена шага
            double stepCoast = 1.0 / cmd.minResolution;
            // ошибка составит
            AngleConverter AngleError = toGo - stepCoast * stepCount;

            bool Finish = true; // доехали

            // если ошибка больше 1", то доехать на высоком разрешении
            if (AngleError.getSec() > 1.0)
            {
                moveCommand fixerrcmd = {
                    RESOLUTION_MAX,
                    cmd.destination,
                    false
                };

                mutex.lock();
                queue.push_front(fixerrcmd);
                mutex.unlock();
                Finish = false;
            }

            // установка нужного разрешения
            setResolution(cmd.minResolution);

            // выполняем
            controlPrepare();

            setStep(false); //reset STEP

            bool side = true;
            if (stepCount < 0)
            {
                stepCount = -stepCount;
                side = false;
            }
            setDir(side);

            for (int i = 0; i < stepCount; ++i)
            {
                setStep(true);
                if (verySlowMode)
                    usleep(1);
                setStep(false);
                mutex.lock();
                curentPosition += side ? stepCoast : -stepCoast;
                emit AngleChanged(curentPosition);
                if (fStop)
                {
                    mutex.unlock();
                    Finish = false;
                    break;
                }
                mutex.unlock();
            }
            controlFinish();
            if (Finish)
                emit MoveFinished();

            lastSide = side;
        }
    }
}

void MotorEncoderControl::doAngle(double dAngle, double minRes, bool forceSlow) // встать на угол dAngle
{
    moveCommand cmd = {minRes, dAngle, forceSlow};

    mutex.lock();
    queue.enqueue(cmd);
    mutex.unlock();
    if (!isRunning())
    {
        start();
        setPriority(TimeCriticalPriority);
    }
    LOG_DEBUG(trUtf8("Destination %1 (res=%2) pushed").arg(
                  AngleConverter(dAngle).toString()).arg(
                  minRes));
}

void MotorEncoderControl::doAngle(const AngleConverter &dAngle, double minRes, bool forceSlow)
{
    doAngle(dAngle.getAngle(), minRes, forceSlow);
}
