
#include <cmath>
#include <QObject>

#include "angleconverter.h"
#include "stockAnglescaledraw.h"

StockAngleScaleDraw::StockAngleScaleDraw()
    : QwtScaleDraw()
{
}

QwtText StockAngleScaleDraw::label(double v) const
{
    return AngleConverter(v).toString();
}


