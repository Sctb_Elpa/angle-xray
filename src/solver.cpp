#include <QString>

#include "solver.h"

double solver::solve(double input)
{
    return input;
}

void solver::reset()
{
}

QMap<QString, double> solver::getCoeffs() const
{
    return QMap<QString, double>();
}

bool solver::setCoeffs(const QMap<QString, double> &coeffs)
{
    return false;
}
