
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <mylog/mylog.h>

#include "angleeditor.h"
#include "motorencodercontrol.h"
#include "angleconverter.h"

#include "manualmovewindow.h"

ManualMoveWindow::ManualMoveWindow(MotorEncoderControl *mec, QWidget *parent) :
    QDialog(parent)
{
    this->mec = mec;
    QVBoxLayout *rootlayout = new QVBoxLayout;

    QLabel *currentPosLabel = new QLabel(trUtf8("<p align=\"center\">Текущая позиция</p>"));
    currentposEdit = new AngleEditor;
    QLabel *DestPosLabel = new QLabel(trUtf8("<p align=\"center\">Позиция назначения</p>"));
    destinationEdit = new AngleEditor;

    QHBoxLayout *buttonlayout = new QHBoxLayout;
    GoButton = new QPushButton(trUtf8("Двигаться"));
    stopButton = new QPushButton(trUtf8("Стоп"));
    ResetButton = new QPushButton(trUtf8("Сброс"));
    buttonlayout->addWidget(stopButton);
    buttonlayout->addWidget(ResetButton);

    rootlayout->addWidget(currentPosLabel);
    rootlayout->addWidget(currentposEdit);
    rootlayout->addWidget(DestPosLabel);
    rootlayout->addWidget(destinationEdit);
    rootlayout->addWidget(GoButton);
    rootlayout->addLayout(buttonlayout);

    setWindowTitle(trUtf8("Ручное управление"));

    connect(mec, SIGNAL(AngleChanged(double)), this, SLOT(currentPosChanged(double)));
    connect(GoButton, SIGNAL(clicked()), this, SLOT(GoSlot()));
    connect(stopButton, SIGNAL(clicked()), mec, SLOT(stop()));
    connect(ResetButton, SIGNAL(clicked()), this, SLOT(resetSlot()));
    connect(currentposEdit, SIGNAL(textEdited(QString)), this, SLOT(currentPosEdited()));
    connect(destinationEdit, SIGNAL(enterPressed()), this, SLOT(GoSlot()));

    setLayout(rootlayout);
}

void ManualMoveWindow::currentPosChanged(double pos)
{
    AngleConverter conv(pos);
    currentposEdit->setValue(conv);
}

void ManualMoveWindow::GoSlot()
{
    mec->doAngle(destinationEdit->value().getAngle());
}

void ManualMoveWindow::resetSlot()
{
    AngleConverter c(0.0);
    destinationEdit->setValue(c);
    GoSlot();
}

void ManualMoveWindow::currentPosEdited()
{
    mec->SetCurentPos(currentposEdit->value().getAngle());
}

