/*
 * ParameterGrig.cpp
 *
 *  Created on: 30.01.2012
 *      Author: shiloxyz
 */

#include <QLabel>
#include <QSlider>
#include <QSpinBox>

#include "ParameterGrid.h"

ParameterGrid::ParameterGrid(QString& labelText, int DefaultValue)
{
    label = new QLabel(labelText, this);
    comonconstructor(DefaultValue);
}

ParameterGrid::ParameterGrid(QString labelText, int DefaultValue)
{
    label = new QLabel(labelText, this);
    comonconstructor(DefaultValue);
}

ParameterGrid::~ParameterGrid()
{
}

bool ParameterGrid::SetLimits(int Min, int Max)
{
    if (Max <= Min)
        return 1;
    slider->setMinimum(Min);
    spinbox->setMinimum(Min);
    slider->setMaximum(Max);
    spinbox->setMaximum(Max);
    return 0;
}

void ParameterGrid::SetValue(int Val)
{
    int min = slider->minimum();
    int max = slider->maximum();
    if ((Val >= min) && (Val <= max))
        slider->setValue(Val);
}



void ParameterGrid::comonconstructor(int DefaultValue)
{
    spinbox = new QSpinBox;
    slider = new QSlider(Qt::Horizontal);

    connect(slider, SIGNAL(valueChanged(int)), spinbox, SLOT(setValue(int)));
    connect(spinbox, SIGNAL(valueChanged(int)), slider, SLOT(setValue(int)));

    connect(slider, SIGNAL(valueChanged(int)), this, SIGNAL(valueChanged(int)));
    spinbox->setValue(DefaultValue);
}

void ParameterGrid::setEnabled(bool fEnabled)
{
    slider->setEnabled(fEnabled);
    spinbox->setEnabled(fEnabled);
}

void ParameterGrid::setEnabled(int iEnabled)
{
    this->setEnabled(iEnabled != 0);
}

void ParameterGrid::setSingleStep(int step)
{
    slider->setSingleStep(step);
    spinbox->setSingleStep(step);
}

int ParameterGrid::value(void)
{
    return spinbox->value();
}
