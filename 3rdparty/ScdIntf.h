/* header for ScdIntf.dll */

#ifndef SCDINTF_H_
#define SCDINTF_H_

#ifdef __cplusplus
extern "C" {
#endif

// структура, указатель на каторую используется как объект
struct Scd;

// режим измерения
enum Mode
{
	smPulseCount = 0, // Режим измерения числа импульсов за указанное время
	smTimeCount	= 1, // Режим измерения времени на набор указанного числа импульсов
	smIntensity = 2 // Режим измерения интенсивности
};

typedef enum
{
	eScdDllOk = 0, // Успешное завершение
	eScdDllInvalidDevice = 1, //Переданный хэндл (handle) не является хэндлом детектора.
	eScdDllThreshOutOfRange = 2, //Значение порога вне диапазона 0..1023
	eScdDllUppThreshLessLowThresh = 3, //Нижний порог больше верхнего.
	eScdDllNumberOfPulsesOutOfRange = 4, //Значение числа импульсов вне диапазона 1..1000000000
	eScdDllVoltageOutOfRange = 5, // Значение напряжения вне диапазона 0..818 Вольт
	eScdDllExposureOutOfRange = 6, // Значение времени экспозиции вне диапазона 0.001.. 1000 сек. (на самом деле максимальное время экспозиции 536,85)
	eScdDllTimeConstOutOfRange = 7, // Константа времени для интенсивности вне диапазона 0.01..1000 сек
	eScdDllCanNotOpenCOM = 8 //Не удаётся открыть указанный COM порт (он не существует или уже занят).
} errCode;

// callbacks
// будет вызвана сразу после начала/завершения измерения
typedef void (*TStartStopProc)(struct Scd* ScdHandle) __attribute__((__stdcall__));
// вызывается при каждом получении данных
typedef void (*TDataProc)(struct Scd* ScdHandle, double time, int PulseNo) __attribute__((__stdcall__));

//connect
// в ScdHandle будет записан укаатель на созданную внутри либы структур
// поля структуры не доступны, разумеется.
errCode ScdConnect(int ComPort, struct Scd** ScdHandle) __attribute__((__stdcall__));
// дисконнект
errCode ScdDisconnect(struct Scd* ScdHandle) __attribute__((__stdcall__));
//регистраторы callback'ов
errCode RegisterAcqStartProc(struct Scd* ScdHandle, TStartStopProc Proc) __attribute__((__stdcall__));
errCode RegisterAcqStopProc(struct Scd* ScdHandle, TStartStopProc Proc) __attribute__((__stdcall__));
errCode RegisterAcqDataProc(struct Scd* ScdHandle, TDataProc Proc) __attribute__((__stdcall__));
// переключение режимов
errCode SetMode(struct Scd* ScdHandle, enum Mode TScdMode) __attribute__((__stdcall__));
// узнать текущий режим (верентся в TScdMode)
errCode GetMode(struct Scd* ScdHandle, enum Mode* TScdMode) __attribute__((__stdcall__));

// инициализация PulseCount mode
// Время экспозиции (0.001 - 536.85) [c]
errCode SetExposureTime(struct Scd* ScdHandle, double Value) __attribute__((__stdcall__));
// Узнать текущее время экспозиции
errCode GetExposureTime(struct Scd* ScdHandle, double* Buf) __attribute__((__stdcall__));

//Инициализация TimeCount mode
// Количество импульсов (1 - 1000000000) [шт]
errCode SetNumberOfPulses(struct Scd* ScdHandle, int Value) __attribute__((__stdcall__));
// Узнать текущее колицество импульсов
errCode GetNumberOfPulses(struct Scd* ScdHandle, int* Buf) __attribute__((__stdcall__));

//Инициализация Intensity mode
// время замера - 0,1с
//Постоянная времени, отвечающая за сглаживание статистических выбросов
// 0.1 - 1000 [c]
#define TIME_CONST_MIN      (0.1)
#define TIME_CONST_MAX      (1000)

errCode SetTimeConst(struct Scd* ScdHandle, double Value) __attribute__((__stdcall__));
//Узнать текущее значение постоянной времени
errCode GetTimeConst(struct Scd* ScdHandle, double* Buf) __attribute__((__stdcall__));

// Все настройки нужно делать при остановленом измерениии
// Зпуск
errCode StartMeasurement(struct Scd* ScdHandle) __attribute__((__stdcall__));
// Стоп
errCode StopMeasurement(struct Scd* ScdHandle) __attribute__((__stdcall__));

// установка питания (0 - 818) [c]
#define VOLTAGE_MIN         (0)
#define VOLTAGE_MAX         (818)

errCode SetVoltage(struct Scd* ScdHandle, int Value) __attribute__((__stdcall__));
// Узнать текущее напряжение
errCode GetVoltage(struct Scd* ScdHandle, int* Buf) __attribute__((__stdcall__));

// окно энергий регистрируемых квантов
// нижний всегда менше верхнего хотя-бы на 1
// Значения относительны (0 - 1023)
#define THRESHOLD_MIN       (0)
#define THRESHOLD_MAX       (1023)

errCode SetLowerThreshold(struct Scd* ScdHandle, int Value) __attribute__((__stdcall__));
errCode SetUpperThreshold(struct Scd* ScdHandle, int Value) __attribute__((__stdcall__));
// Узнать текущее значение порогов
errCode GetLowerThreshold(struct Scd* ScdHandle, int* Buf) __attribute__((__stdcall__));
errCode GetUpperThreshold(struct Scd* ScdHandle, int* Buf) __attribute__((__stdcall__));

// интегральный режим, значение UpperThreshold == +Inf
errCode SetIntegralMode(struct Scd* ScdHandle, bool Enable) __attribute__((__stdcall__));
//Узнать текущее состояние интегрального режима
errCode GetIntegralMode(struct Scd* ScdHandle, bool* Buf) __attribute__((__stdcall__));

// Звук
// вкл/выкл
errCode SetSoundMode(struct Scd* ScdHandle, bool Enable) __attribute__((__stdcall__));
// Узнать текущее состояние звука
errCode GetSoundMode(struct Scd* ScdHandle, bool* Buf) __attribute__((__stdcall__));

//Коррекция просчетов
// Доступна только в режиме PulseCount
errCode SetCorrectionMode(struct Scd* ScdHandle, bool Enable) __attribute__((__stdcall__));
// Узнать текущее состояние звука
errCode GetCorrectionMode(struct Scd* ScdHandle, bool* Buf) __attribute__((__stdcall__));

// Сброс всех настроек
errCode ResetCard(struct Scd* ScdHandle) __attribute__((__stdcall__));

// прямой опрос
// готов ли следующий отсчет? IsReady == 0 - не готовы
errCode IsDataReady(struct Scd* ScdHandle, int* IsReady) __attribute__((__stdcall__));
// считать результат последнего измерения
errCode LastMeasureData(struct Scd* ScdHandle, double* Time, int* PulseNo) __attribute__((__stdcall__));
// после вызова LastMeasureData следует вызывать..
errCode DropDataReady(struct Scd* ScdHandle) __attribute__((__stdcall__));

#ifdef __cplusplus
}
#endif

#endif
